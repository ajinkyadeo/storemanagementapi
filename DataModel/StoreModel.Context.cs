﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class StoreManagementEntities : DbContext
    {
        public StoreManagementEntities()
            : base("name=StoreManagementEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<MenuMaster> MenuMasters { get; set; }
        public DbSet<MstCustomer> MstCustomers { get; set; }
        public DbSet<MstDistributor> MstDistributors { get; set; }
        public DbSet<MstProduct> MstProducts { get; set; }
        public DbSet<MstRole> MstRoles { get; set; }
        public DbSet<MstSetting> MstSettings { get; set; }
        public DbSet<MstUser> MstUsers { get; set; }
        public DbSet<RoleMenu> RoleMenus { get; set; }
        public DbSet<SaleOrder> SaleOrders { get; set; }
    }
}
