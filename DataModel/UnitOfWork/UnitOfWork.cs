﻿#region Using Namespaces...

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Data.Entity.Validation;
using DataModel.GenericRepository;

#endregion

namespace DataModel.UnitOfWork
{
    /// <summary>
    /// Unit of Work class responsible for DB transactions
    /// </summary>
    public class UnitOfWork : IDisposable, IUnitOfWork
    {
        #region Private member variables...

        private readonly StoreManagementEntities _context = null;

     
        private GenericRepository<MstUser> _mstUserRepository;
        private GenericRepository<MstRole> _mstRoleRepository;
        private GenericRepository<MstSetting> _mstSettingRepository;
        private GenericRepository<MstProduct> _mstProductRepository;
        private GenericRepository<MstDistributor> _mstDistributorRepository;
        private GenericRepository<Inventory> _inventoryRepository;
        private GenericRepository<MstCustomer> _mstCustomerRepository;
        private GenericRepository<SaleOrder> _saleOrderRepository;
        private GenericRepository<RoleMenu> _roleMenuRepository;
        private GenericRepository<MenuMaster> _menuMasterRepository;




        #endregion

        public UnitOfWork()
        {
            _context = new StoreManagementEntities();
        }

        #region Public Repository Creation properties...
        public GenericRepository<MenuMaster> MenuMasterRepository
        {
            get
            {
                if (this._menuMasterRepository == null)
                    this._menuMasterRepository = new GenericRepository<MenuMaster>(_context);
                return _menuMasterRepository;
            }
        }
        public GenericRepository<RoleMenu> RoleMenuRepository
        {
            get
            {
                if (this._roleMenuRepository == null)
                    this._roleMenuRepository = new GenericRepository<RoleMenu>(_context);
                return _roleMenuRepository;
            }
        }
        public GenericRepository<SaleOrder> SaleOrderRepository
        {
            get
            {
                if (this._saleOrderRepository == null)
                    this._saleOrderRepository = new GenericRepository<SaleOrder>(_context);
                return _saleOrderRepository;
            }
        }
        public GenericRepository<MstCustomer> MstCustomerRepository
        {
            get
            {
                if (this._mstCustomerRepository == null)
                    this._mstCustomerRepository = new GenericRepository<MstCustomer>(_context);
                return _mstCustomerRepository;
            }
        }
        public GenericRepository<Inventory> InventoryRepository
        {
            get
            {
                if (this._inventoryRepository == null)
                    this._inventoryRepository = new GenericRepository<Inventory>(_context);
                return _inventoryRepository;
            }
        }
        public GenericRepository<MstDistributor> MstDistributorRepository
        {
            get
            {
                if (this._mstDistributorRepository == null)
                    this._mstDistributorRepository = new GenericRepository<MstDistributor>(_context);
                return _mstDistributorRepository;
            }
        }
        public GenericRepository<MstProduct> MstProductRepository
        {
            get
            {
                if (this._mstProductRepository == null)
                    this._mstProductRepository = new GenericRepository<MstProduct>(_context);
                return _mstProductRepository;
            }
        }
        public GenericRepository<MstSetting> MstSettingRepository
        {
            get
            {
                if (this._mstSettingRepository == null)
                    this._mstSettingRepository = new GenericRepository<MstSetting>(_context);
                return _mstSettingRepository;
            }
        }
        public GenericRepository<MstRole> MstRoleRepository
        {
            get
            {
                if (this._mstRoleRepository == null)
                    this._mstRoleRepository = new GenericRepository<MstRole>(_context);
                return _mstRoleRepository;
            }
        }
        public GenericRepository<MstUser> MstUserRepository
        {
            get
            {
                if (this._mstUserRepository == null)
                    this._mstUserRepository = new GenericRepository<MstUser>(_context);
                return _mstUserRepository;
            }
        }
      



        #endregion

        #region Public member methods...
        /// <summary>
        /// Save method.
        /// </summary>
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {

                var outputLines = new List<string>();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.Add(string.Format("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:", DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                }
                System.IO.File.AppendAllLines(@"C:\errors.txt", outputLines);

                throw e;
            }

        }

        #endregion

        #region Implementing IDiosposable...

        #region private dispose variable declaration...
        private bool disposed = false;
        #endregion

        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}