﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Product")]
    public class MstProductController : ApiController
    {

        #region Private variable.

        private readonly IMstProductServices _ProductServices;

        public ResponseWrapper<MstProductEntity> objResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public MstProductController(IMstProductServices userServices)
        {
            _ProductServices = userServices;
        }

        #endregion

        [DeflateCompression]
        [GET("GetById/{id}")]
        public ResponseWrapper<MstProductEntity> GetById(int id)
        {
            var proeprty = _ProductServices.GetById(id);
            List<MstProductEntity> ProductEntityList = new List<MstProductEntity>();
            if (proeprty != null)
            {
                ProductEntityList.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<MstProductEntity>(ProductEntityList, HttpStatusCode.OK, null, 0);
            }
            new ApiDataException(1001, "No Product found for this id.", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstProductEntity>(null, HttpStatusCode.InternalServerError, "No Product found for this id.", 1);
        }

        [DeflateCompression]
        [GET("GetAllProduct")]
        public ResponseWrapper<MstProductEntity> GetAllProduct()
        {
            var proeprty = _ProductServices.GetAllProduct();
            var proeprtyEntities = proeprty as List<MstProductEntity> ?? proeprty.ToList();
            if (proeprtyEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstProductEntity>(proeprtyEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Product not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstProductEntity>(null, HttpStatusCode.InternalServerError, "Product not found.", 1);
        }

        [DeflateCompression]
        [GET("GetAllProductByInventory")]
        public ResponseWrapper<MstProductEntity> GetAllProductByInventory()
        {
            var proeprty = _ProductServices.GetAllProductByInventory();
            var proeprtyEntities = proeprty as List<MstProductEntity> ?? proeprty.ToList();
            if (proeprtyEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstProductEntity>(proeprtyEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Product not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstProductEntity>(null, HttpStatusCode.InternalServerError, "Product not found.", 1);
        }

    }
}
