﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Distributor")]
    public class MstDistributorController : ApiController
    {

        #region Private variable.

        private readonly IMstDistributorServices _DistributorServices;

        public ResponseWrapper<MstDistributorEntity> objResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public MstDistributorController(IMstDistributorServices userServices)
        {
            _DistributorServices = userServices;
        }

        #endregion

        [DeflateCompression]
        [GET("GetById/{id}")]
        public ResponseWrapper<MstDistributorEntity> GetById(int id)
        {
            var proeprty = _DistributorServices.GetById(id);
            List<MstDistributorEntity> DistributorEntityList = new List<MstDistributorEntity>();
            if (proeprty != null)
            {
                DistributorEntityList.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(DistributorEntityList, HttpStatusCode.OK, null, 0);
            }
            new ApiDataException(1001, "No Distributor found for this id.", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(null, HttpStatusCode.InternalServerError, "No Distributor found for this id.", 1);
        }

        [DeflateCompression]
        [GET("GetAllDistributor")]
        public ResponseWrapper<MstDistributorEntity> GetAllDistributor()
        {
            var proeprty = _DistributorServices.GetAllDistributor();
            var proeprtyEntities = proeprty as List<MstDistributorEntity> ?? proeprty.ToList();
            if (proeprtyEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(proeprtyEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Distributor not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(null, HttpStatusCode.InternalServerError, "Distributor not found.", 1);
        }

        [DeflateCompression]
        [GET("GetAllDistributorByProduct/{productId}")]
        public ResponseWrapper<MstDistributorEntity> GetAllDistributorByProduct(int productId)
        {
            var proeprty = _DistributorServices.GetAllDistributorByProduct(productId);
            var proeprtyEntities = proeprty as List<MstDistributorEntity> ?? proeprty.ToList();
            if (proeprtyEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(proeprtyEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Distributor not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstDistributorEntity>(null, HttpStatusCode.InternalServerError, "Distributor not found.", 1);
        }


    }
}
