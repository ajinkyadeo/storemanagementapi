﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Inventorys")]
    public class InventoryController : ApiController
    {

        #region Private variable.

        private readonly IInventoryServices _InventoryServices;

        public ResponseWrapper<InventoryEntity> objResponseWrapper = null;
        //public ResponseWrapper<InventorySuperEntity> objInventoryResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public InventoryController(IInventoryServices InventoryServices)
        {
            _InventoryServices = InventoryServices;
        }

        #endregion

      
       

        [DeflateCompression]
        [GET("GetInventoryById/{id}")]
        [AllowAnonymous]
        public ResponseWrapper<InventoryEntity> GetInventoryById(int id)
        {
            List<InventoryEntity> lstInventory = new List<InventoryEntity>();

            var Inventory = _InventoryServices.GetByInventoryId(id);

            if (Inventory != null)
            {
                lstInventory.Add(Inventory);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(lstInventory, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Inventory found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(null, HttpStatusCode.OK, "No Inventory found for this id.", 1);
            }
        }

      
        [DeflateCompression]
        [GET("GetAllInventorys")]
        public ResponseWrapper<InventoryEntity> GetAllInventorys()
        {
            var roles = _InventoryServices.GetAllInventorys();
            var roleEntities = roles as List<InventoryEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Inventorys not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<InventoryEntity>(null, HttpStatusCode.InternalServerError, "Inventorys not found.", 1);
        }

        [DeflateCompression]
        [POST("UpdateInventory")]
        [AllowAnonymous]
        public bool UpdateInventory([FromBody] InventoryEntity propertyEntity)
        {
            bool isUpdated = _InventoryServices.UpdateInventory(propertyEntity);

            return isUpdated;
        }

    
        [DeflateCompression]
        [GET("GetDeleteInventory/{id}")]
        [AllowAnonymous]
        public bool GetDeleteInventory(int id)
        {
            bool isUpdated = _InventoryServices.GetDeleteInventory(id);

            return isUpdated;
        }
        [DeflateCompression]
        [POST("CreateInventory")]
        [AllowAnonymous]
        public ResponseWrapper<InventoryEntity> CreateInventory([FromBody] InventoryEntity propertyEntity)
        {
            List<InventoryEntity> lstproperty = new List<InventoryEntity>();

            var proeprty = _InventoryServices.CreateInventory(propertyEntity);
            if (proeprty != null)
            {
                lstproperty.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(lstproperty, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Inventory found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(null, HttpStatusCode.OK, "No Inventory found for this id.", 1);
            }
        }
        [DeflateCompression]
        [GET("GetByProductId/{productid}")]
        [AllowAnonymous]
        public ResponseWrapper<InventoryEntity> GetByProductId(int productid)
        {
            List<InventoryEntity> lstInventory = new List<InventoryEntity>();

            var Inventory = _InventoryServices.GetByProductId(productid);

            if (Inventory != null)
            {
                lstInventory.Add(Inventory);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(lstInventory, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Inventory found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(null, HttpStatusCode.OK, "No Inventory found for this id.", 1);
            }
        }

        [DeflateCompression]
        [GET("GetAllInventorysByProductId/{productid}/{distributorId}")]
        [AllowAnonymous]
        public ResponseWrapper<InventoryEntity> GetAllInventorysByProductId(int productid, int distributorId)
        {
            List<InventoryEntity> lstInventory = new List<InventoryEntity>();

            var Inventory = _InventoryServices.GetAllInventorysByProductId(productid, distributorId);

            if (Inventory != null)
            {
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(Inventory, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Inventory found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<InventoryEntity>(null, HttpStatusCode.OK, "No Inventory found for this id.", 1);
            }
        }
    }
}
