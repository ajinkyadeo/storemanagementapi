﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Users")]
    public class UserController : ApiController
    {

        #region Private variable.

        private readonly IMstUserServices _userServices;

        public ResponseWrapper<MstUserEntity> objResponseWrapper = null;
        //public ResponseWrapper<UserSuperEntity> objUserResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public UserController(IMstUserServices userServices)
        {
            _userServices = userServices;
        }

        #endregion

      
        [DeflateCompression]
        [GET("Login/{userName}/{password}")]
        public ResponseWrapper<MstUserEntity> GetLogin(string userName, string password)
        {
            List<MstUserEntity> lstUser = new List<MstUserEntity>();

            var user = _userServices.GetUserByUserNamePassword(userName, password);
            if (user != null)
            {
               
                lstUser.Add(user);
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(lstUser, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No user found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "No user found for this id.", 1);
            }
        }

        [DeflateCompression]
        [GET("GetUserById/{id}")]
        [AllowAnonymous]
        public ResponseWrapper<MstUserEntity> GetUserById(int id)
        {
            List<MstUserEntity> lstUser = new List<MstUserEntity>();

            var user = _userServices.GetByUserId(id);

            if (user != null)
            {
                lstUser.Add(user);
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(lstUser, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No user found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "No user found for this id.", 1);
            }
        }

        [DeflateCompression]
        [POST("Register")]
        [AllowAnonymous]
        public ResponseWrapper<MstUserEntity> Post([FromBody] MstUserEntity profileEntity)
        {
            bool isExist = _userServices.IsUserExist(profileEntity.UserName);
            if (!isExist)
            {
                List<MstUserEntity> lstUser = new List<MstUserEntity>();

                var user = _userServices.CreateUser(profileEntity);
                if (user != null)
                {
                    lstUser.Add(user);
                    return objResponseWrapper = new ResponseWrapper<MstUserEntity>(lstUser, HttpStatusCode.OK, null, 0);
                }
                else
                {
                    new ApiDataException(1001, "No user found for this id.", HttpStatusCode.NotFound);
                    return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "No user found for this id.", 1);
                }
            }
            else
            {
                new ApiDataException(1001, "User name already exist.", HttpStatusCode.Ambiguous);
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "User name already exist", 1);
            }
        }


    
        [DeflateCompression]
        [GET("GetAllUsers")]
        public ResponseWrapper<MstUserEntity> GetAllUsers()
        {
            var roles = _userServices.GetAllUsers();
            var roleEntities = roles as List<MstUserEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstUserEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Users not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.InternalServerError, "Users not found.", 1);
        }

        [DeflateCompression]
        [POST("UpdateUser")]
        [AllowAnonymous]
        public bool UpdateUser([FromBody] MstUserEntity propertyEntity)
        {
            bool isUpdated = _userServices.UpdateUser(propertyEntity);

            return isUpdated;
        }

        [DeflateCompression]
        [POST("UpdateProfile")]
        [AllowAnonymous]
        public bool UpdateProfile([FromBody] MstUserEntity propertyEntity)
        {
            bool isUpdated = _userServices.UpdateProfile(propertyEntity);

            return isUpdated;
        }
       
        [DeflateCompression]
        [POST("UpdatePassword")]
        [AllowAnonymous]
        public bool UpdatePassword([FromBody] MstUserEntity propertyEntity)
        {
            bool isUpdated = _userServices.UpdatePassword(propertyEntity);

            return isUpdated;
        }
      
        [DeflateCompression]
        [GET("GetDeleteUser/{id}")]
        [AllowAnonymous]
        public bool GetDeleteUser(int id)
        {
            bool isUpdated = _userServices.GetDeleteUser(id);

            return isUpdated;
        }
        [DeflateCompression]
        [GET("LockUser/{userId}/{isLock}")]
        [AllowAnonymous]
        public bool LockUser(int userId,bool isLock)
        {
            bool isUpdated = _userServices.LockUser(userId, isLock);

            return isUpdated;
        }

    }
}
