﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/MstSetting")]
    public class MstSettingController : ApiController
    {

        #region Private variable.

        private readonly IMstSettingServices _MstSettingServices;

        public ResponseWrapper<MstSettingEntity> objResponseWrapper = null;
        //public ResponseWrapper<UserSuperEntity> objUserResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public MstSettingController(IMstSettingServices userServices)
        {
            _MstSettingServices = userServices;
        }

        #endregion

        [DeflateCompression]
        [GET("GetById/{id}")]
        //[Authorize(MstSettings = "Manager")]
        public ResponseWrapper<MstSettingEntity> GetById(int id)
        {
            var setting = _MstSettingServices.GetById(id);
            List<MstSettingEntity> MstSettingEntityList = new List<MstSettingEntity>();
            if (setting != null)
            {
                MstSettingEntityList.Add(setting);
                return objResponseWrapper = new ResponseWrapper<MstSettingEntity>(MstSettingEntityList, HttpStatusCode.OK, null, 0);
            }
            new ApiDataException(1001, "No Setting found for this id.", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstSettingEntity>(null, HttpStatusCode.InternalServerError, "No Setting found for this id.", 1);
        }

        [DeflateCompression]
        [GET("GetAllSetting")]
        //[Authorize(MstSettings = "Admin")]
        public ResponseWrapper<MstSettingEntity> GetAllSetting()
        {
            var setting = _MstSettingServices.GetAllSetting();
            var settingEntities = setting as List<MstSettingEntity> ?? setting.ToList();
            if (settingEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstSettingEntity>(settingEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Setting not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstSettingEntity>(null, HttpStatusCode.InternalServerError, "Setting not found.", 1);
        }
        [DeflateCompression]
        [POST("UpdateSetting")]
        [AllowAnonymous]
        public bool UpdateSetting([FromBody] MstSettingEntity propertyEntity)
        {
            bool isUpdated = _MstSettingServices.UpdateSetting(propertyEntity);

            return isUpdated;
        }
    }
}
