﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    [Authorize]
    [RoutePrefix("v1/RoleMenus")]
    public class RoleMenuController : ApiController
    {

        #region Private variable.

        private readonly IRoleMenuServices _RoleMenuServices;

        public ResponseWrapper<RoleMenuEntity> objResponseWrapper = null;
        //public ResponseWrapper<RoleMenuSuperEntity> objRoleMenuResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public RoleMenuController(IRoleMenuServices RoleMenuServices)
        {
            _RoleMenuServices = RoleMenuServices;
        }
        #endregion


        [DeflateCompression]
        [GET("GetRoleMenuById/{id}")]
        //[Authorize]
        [AllowAnonymous]
        public ResponseWrapper<RoleMenuEntity> GetRoleMenuById(int id)
        {
            List<RoleMenuEntity> lstRoleMenu = new List<RoleMenuEntity>();

            var RoleMenu = _RoleMenuServices.GetByRoleMenuId(id);

            if (RoleMenu != null)
            {
                lstRoleMenu.Add(RoleMenu);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(lstRoleMenu, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No RoleMenu found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(null, HttpStatusCode.OK, "No RoleMenu found for this id.", 1);
            }
        }


        // GET api/product
        [DeflateCompression]
        [GET("GetAllRoleMenus")]
        [AllowAnonymous]
        //[Authorize(Roles = "SuperAdmin")]
        public ResponseWrapper<RoleMenuEntity> GetAllRoleMenus()
        {
            var roles = _RoleMenuServices.GetAllRoleMenus();
            var roleEntities = roles as List<RoleMenuEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "RoleMenu data not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(null, HttpStatusCode.InternalServerError, "RoleMenu data not found.", 1);
        }
        [DeflateCompression]
        [POST("CreateRoleMenu")]
        [AllowAnonymous]
        public ResponseWrapper<RoleMenuEntity> Post([FromBody] RoleMenuEntity propertyEntity)
        {
            List<RoleMenuEntity> lstproperty = new List<RoleMenuEntity>();

            var proeprty = _RoleMenuServices.CreateRoleMenu(propertyEntity);
            if (proeprty != null)
            {
                lstproperty.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(lstproperty, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No RoleMenu found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(null, HttpStatusCode.OK, "No RoleMenu found for this id.", 1);
            }
        }

        [DeflateCompression]
        [POST("UpdateRoleMenu")]
        [AllowAnonymous]
        public bool UpdateRoleMenu([FromBody] RoleMenuEntity propertyEntity)
        {
            bool isUpdated = _RoleMenuServices.UpdateRoleMenu(propertyEntity);

            return isUpdated;
        }
        [DeflateCompression]
        [GET("GetDeleteRoleMenu/{id}")]
        [AllowAnonymous]
        public bool GetDeleteRoleMenu(int id)
        {
            bool isUpdated = _RoleMenuServices.GetDeleteRoleMenu(id);

            return isUpdated;
        }
        [DeflateCompression]
        [POST("PostCreateBulkRoleMenu")]
        [AllowAnonymous]
        public bool PostCreateBulkRoleMenu([FromBody] List<RoleMenuEntity> propertyEntity)
        {

            return _RoleMenuServices.CreateBulkRoleMenu(propertyEntity);

        }


        [DeflateCompression]
        [GET("GetRoleMenuByRoleId/{roleId}")]
        //[Authorize]
        [AllowAnonymous]
        public ResponseWrapper<RoleMenuEntity> GetRoleMenuByRoleId(int roleId)
        {
            List<RoleMenuEntity> lstRoleMenu = new List<RoleMenuEntity>();

            var RoleMenu = _RoleMenuServices.GetRoleMenuByRoleId(roleId).ToList();

            if (RoleMenu != null)
            {
                //lstRoleMenu.Add(RoleMenu);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(RoleMenu, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No RoleMenu found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<RoleMenuEntity>(null, HttpStatusCode.OK, "No RoleMenu found for this id.", 1);
            }
        }

    }
}
