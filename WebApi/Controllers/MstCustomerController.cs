﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Customers")]
    public class MstCustomerController : ApiController
    {

        #region Private variable.

        private readonly IMstCustomerServices _CustomerServices;

        public ResponseWrapper<MstCustomerEntity> objResponseWrapper = null;
        //public ResponseWrapper<CustomerSuperEntity> objCustomerResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public MstCustomerController(IMstCustomerServices CustomerServices)
        {
            _CustomerServices = CustomerServices;
        }

        #endregion

      
       

        [DeflateCompression]
        [GET("GetCustomerById/{id}")]
        [AllowAnonymous]
        public ResponseWrapper<MstCustomerEntity> GetCustomerById(int id)
        {
            List<MstCustomerEntity> lstCustomer = new List<MstCustomerEntity>();

            var Customer = _CustomerServices.GetByCustomerId(id);

            if (Customer != null)
            {
                lstCustomer.Add(Customer);
                return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(lstCustomer, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Customer found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(null, HttpStatusCode.OK, "No Customer found for this id.", 1);
            }
        }

      
        [DeflateCompression]
        [GET("GetAllCustomers")]
        public ResponseWrapper<MstCustomerEntity> GetAllCustomers()
        {
            var roles = _CustomerServices.GetAllCustomers();
            var roleEntities = roles as List<MstCustomerEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Customers not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(null, HttpStatusCode.InternalServerError, "Customers not found.", 1);
        }

        [DeflateCompression]
        [POST("UpdateCustomer")]
        [AllowAnonymous]
        public bool UpdateCustomer([FromBody] MstCustomerEntity propertyEntity)
        {
            bool isUpdated = _CustomerServices.UpdateCustomer(propertyEntity);

            return isUpdated;
        }

    
        [DeflateCompression]
        [GET("GetDeleteCustomer/{id}")]
        [AllowAnonymous]
        public bool GetDeleteCustomer(int id)
        {
            bool isUpdated = _CustomerServices.GetDeleteCustomer(id);

            return isUpdated;
        }
        [DeflateCompression]
        [POST("CreateCustomer")]
        [AllowAnonymous]
        public ResponseWrapper<MstCustomerEntity> CreateCustomer([FromBody] MstCustomerEntity propertyEntity)
        {
            List<MstCustomerEntity> lstproperty = new List<MstCustomerEntity>();

            var proeprty = _CustomerServices.CreateCustomer(propertyEntity);
            if (proeprty != null)
            {
                lstproperty.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(lstproperty, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Customer found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MstCustomerEntity>(null, HttpStatusCode.OK, "No Customer found for this id.", 1);
            }
        }

    }
}
