﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/Role")]
    public class RoleController : ApiController
    {

        #region Private variable.

        private readonly IMstRoleServices _roleServices;

        public ResponseWrapper<MstRoleEntity> objResponseWrapper = null;
        //public ResponseWrapper<UserSuperEntity> objUserResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public RoleController(IMstRoleServices userServices)
        {
            _roleServices = userServices;
        }

        #endregion

        [DeflateCompression]
        [GET("GetById/{id}")]
        //[Authorize(Roles = "Manager")]
        public ResponseWrapper<MstRoleEntity> GetById(int id)
        {
            var proeprty = _roleServices.GetById(id);
            List<MstRoleEntity> roleEntityList = new List<MstRoleEntity>();
            if (proeprty != null)
            {
                roleEntityList.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<MstRoleEntity>(roleEntityList, HttpStatusCode.OK, null, 0);
            }
            new ApiDataException(1001, "No Role found for this id.", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstRoleEntity>(null, HttpStatusCode.InternalServerError, "No Role found for this id.", 1);
        }

        [DeflateCompression]
        [GET("GetAllRole")]
        //[Authorize(Roles = "Admin")]
        public ResponseWrapper<MstRoleEntity> GetAllRole()
        {
            var proeprty = _roleServices.GetAllRole();
            var proeprtyEntities = proeprty as List<MstRoleEntity> ?? proeprty.ToList();
            if (proeprtyEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MstRoleEntity>(proeprtyEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Role not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MstRoleEntity>(null, HttpStatusCode.InternalServerError, "Role not found.", 1);
        }
        
    }
}
