﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    //[Authorize]
    [AllowAnonymous]
    [RoutePrefix("v1/SaleOrders")]
    public class SaleOrderController : ApiController
    {

        #region Private variable.

        private readonly ISaleOrderServices _SaleOrderServices;

        public ResponseWrapper<SaleOrderEntity> objResponseWrapper = null;
        //public ResponseWrapper<SaleOrderSuperEntity> objSaleOrderResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public SaleOrderController(ISaleOrderServices SaleOrderServices)
        {
            _SaleOrderServices = SaleOrderServices;
        }

        #endregion

      
       

        [DeflateCompression]
        [GET("GetSaleOrderById/{id}")]
        [AllowAnonymous]
        public ResponseWrapper<SaleOrderEntity> GetSaleOrderById(int id)
        {
            List<SaleOrderEntity> lstSaleOrder = new List<SaleOrderEntity>();

            var SaleOrder = _SaleOrderServices.GetBySaleOrderId(id);

            if (SaleOrder != null)
            {
                lstSaleOrder.Add(SaleOrder);
                return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(lstSaleOrder, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No SaleOrder found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(null, HttpStatusCode.OK, "No SaleOrder found for this id.", 1);
            }
        }

      
        [DeflateCompression]
        [GET("GetAllSaleOrders")]
        public ResponseWrapper<SaleOrderEntity> GetAllSaleOrders()
        {
            var roles = _SaleOrderServices.GetAllSaleOrders();
            var roleEntities = roles as List<SaleOrderEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "SaleOrders not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(null, HttpStatusCode.InternalServerError, "SaleOrders not found.", 1);
        }

        [DeflateCompression]
        [POST("UpdateSaleOrder")]
        [AllowAnonymous]
        public bool UpdateSaleOrder([FromBody] SaleOrderEntity propertyEntity)
        {
            bool isUpdated = _SaleOrderServices.UpdateSaleOrder(propertyEntity);

            return isUpdated;
        }

    
        [DeflateCompression]
        [GET("GetDeleteSaleOrder/{id}")]
        [AllowAnonymous]
        public bool GetDeleteSaleOrder(int id)
        {
            bool isUpdated = _SaleOrderServices.GetDeleteSaleOrder(id);

            return isUpdated;
        }
        [DeflateCompression]
        [POST("CreateSaleOrder")]
        [AllowAnonymous]
        public ResponseWrapper<SaleOrderEntity> CreateSaleOrder([FromBody] SaleOrderEntity propertyEntity)
        {
            List<SaleOrderEntity> lstproperty = new List<SaleOrderEntity>();

            var proeprty = _SaleOrderServices.CreateSaleOrder(propertyEntity);
            if (proeprty != null)
            {
                lstproperty.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(lstproperty, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No SaleOrder found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<SaleOrderEntity>(null, HttpStatusCode.OK, "No SaleOrder found for this id.", 1);
            }
        }

    }
}
