﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using BusinessEntities;
using BusinessServices;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Web;
using System;
using System.IO;
using WebApi.Filters;

namespace WebApi.Controllers
{

    [Authorize]
    [RoutePrefix("v1/Menu")]
    public class MenuController : ApiController
    {

        #region Private variable.

        private readonly IMenuMasterServices _MenuMasterServices;

        public ResponseWrapper<MenuMasterEntity> objResponseWrapper = null;
        //public ResponseWrapper<MenuMasterSuperEntity> objMenuMasterResponseWrapper = null;

        #endregion

        #region Public Constructor

        /// <summary>
        /// Public constructor to initialize product service instance
        /// </summary>
        public MenuController(IMenuMasterServices MenuMasterServices)
        {
            _MenuMasterServices = MenuMasterServices;
        }
        #endregion


        // GET api/product
        [DeflateCompression]
        [GET("GetMenuByRoleId/{roleId}")]
        [AllowAnonymous]
        //[Authorize(Roles = "SuperAdmin")]
        public ResponseWrapper<MenuMasterEntity> GetMenuByRoleId(int roleId)
        {
            var roles = _MenuMasterServices.GetMenuByRoleId(roleId);
            var roleEntities = roles as List<MenuMasterEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Menu data not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(null, HttpStatusCode.InternalServerError, "Menu data not found.", 1);
        }
        [DeflateCompression]
        [GET("GetMenuMasterById/{id}")]
        //[Authorize]
        [AllowAnonymous]
        public ResponseWrapper<MenuMasterEntity> GetMenuMasterById(int id)
        {
            List<MenuMasterEntity> lstMenuMaster = new List<MenuMasterEntity>();

            var MenuMaster = _MenuMasterServices.GetByMenuMasterId(id);

            if (MenuMaster != null)
            {
                lstMenuMaster.Add(MenuMaster);
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(lstMenuMaster, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No MenuMaster found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(null, HttpStatusCode.OK, "No MenuMaster found for this id.", 1);
            }
        }


        // GET api/product
        [DeflateCompression]
        [GET("GetAllMenuMasters")]
        [AllowAnonymous]
        //[Authorize(Roles = "SuperAdmin")]
        public ResponseWrapper<MenuMasterEntity> GetAllMenuMasters()
        {
            var roles = _MenuMasterServices.GetAllMenuMasters();
            var roleEntities = roles as List<MenuMasterEntity> ?? roles.ToList();
            if (roleEntities.Any())
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(roleEntities, HttpStatusCode.OK, null, 0);
            new ApiDataException(1000, "Menu data not found", HttpStatusCode.NotFound);
            return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(null, HttpStatusCode.InternalServerError, "Menu data not found.", 1);
        }
        [DeflateCompression]
        [POST("CreateMenuMaster")]
        [AllowAnonymous]
        public ResponseWrapper<MenuMasterEntity> Post([FromBody] MenuMasterEntity propertyEntity)
        {
            List<MenuMasterEntity> lstproperty = new List<MenuMasterEntity>();

            var proeprty = _MenuMasterServices.CreateMenuMaster(propertyEntity);
            if (proeprty != null)
            {
                lstproperty.Add(proeprty);
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(lstproperty, HttpStatusCode.OK, null, 0);
            }
            else
            {
                new ApiDataException(1001, "No Menu found for this id.", HttpStatusCode.NotFound);
                return objResponseWrapper = new ResponseWrapper<MenuMasterEntity>(null, HttpStatusCode.OK, "No Menu found for this id.", 1);
            }
        }

        [DeflateCompression]
        [POST("UpdateMenuMaster")]
        [AllowAnonymous]
        public bool UpdateMenuMaster([FromBody] MenuMasterEntity propertyEntity)
        {
            bool isUpdated = _MenuMasterServices.UpdateMenuMaster(propertyEntity);

            return isUpdated;
        }
        [DeflateCompression]
        [GET("GetDeleteMenuMaster/{id}")]
        [AllowAnonymous]
        public bool GetDeleteMenuMaster(int id)
        {
            bool isUpdated = _MenuMasterServices.GetDeleteMenuMaster(id);

            return isUpdated;
        }

    }
}
