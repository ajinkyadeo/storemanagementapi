﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WebApi.Helpers
{
    public class ResponseWrapper<T> where T : class
    {
        public ResponseWrapper(List<T> lstitems, HttpStatusCode Status, string ErrorDetail, int isError)
        {
            StatusCode = Status;

            DetailsError = ErrorDetail;

            if (lstitems != null)
            {
                ReturnObject = lstitems;
            }
            else
            {
                ReturnObject = null;
            }

            IsError = isError;
        }

        public HttpStatusCode StatusCode { get; set; }

        public string DetailsError { get; set; }

        public List<T> ReturnObject { get; set; }

        public int IsError { get; set; }

    }
}