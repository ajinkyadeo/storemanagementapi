﻿using BusinessEntities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApi.Helpers;

namespace WebApi.Models
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (UserMasterRepository _repo = new UserMasterRepository())
            {
                string password = WebUtility.UrlDecode(context.Password);
                if (!context.Scope[0].Equals("1"))
                {
                    ResponseWrapper<MstUserEntity> user = _repo.ValidateUser(context.UserName, password);
                    if (user.ReturnObject == null)
                    {
                        context.SetError("invalid_grant", user.DetailsError);
                        return;
                    }
                 
                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim(ClaimTypes.Role, user.ReturnObject[0].MstRole.RoleName));
                    identity.AddClaim(new Claim(ClaimTypes.Name, user.ReturnObject[0].UserName));
                    
                    var additionalData = new AuthenticationProperties(new Dictionary<string, string>{
                        {
                            "role",Newtonsoft.Json.JsonConvert.SerializeObject(user.ReturnObject[0].MstRole.RoleName)
                        }
                    });
                    var token = new AuthenticationTicket(identity, additionalData);
                    context.Validated(token);
                    //context.Validated(identity);
                }
                else
                {
                    //var adminuser = _repo.ValidateAdminUser(context.UserName, password);
                    //if (adminuser.ReturnObject == null)
                    //{
                    //    context.SetError("invalid_grant", adminuser.DetailsError);
                    //    return;
                    //}
                    //var identityAdmin = new ClaimsIdentity(context.Options.AuthenticationType);
                    //identityAdmin.AddClaim(new Claim(ClaimTypes.Role, "SuperAdmin"));
                    //identityAdmin.AddClaim(new Claim("Email", adminuser.ReturnObject[0].Email));
                    //identityAdmin.AddClaim(new Claim("Id", adminuser.ReturnObject[0].Password != null ? adminuser.ReturnObject[0].Password : ""));
                    ////context.Validated(identityAdmin);
                    //var additionalData = new AuthenticationProperties(new Dictionary<string, string>{
                    //    {
                    //        "role",Newtonsoft.Json.JsonConvert.SerializeObject("SuperAdmin")
                    //    }
                    //});
                    //var token = new AuthenticationTicket(identityAdmin, additionalData);
                    //context.Validated(token);
                    ////context.Validated(identityAdmin);
                    ////return;
                }
            }
        }
        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }
    }
}