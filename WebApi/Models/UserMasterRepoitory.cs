﻿using BusinessEntities;
using DataModel;
using System;
using System.Linq;
using AutoMapper;
using WebApi.ErrorHelper;
using WebApi.Helpers;
using System.Net;
using System.Collections.Generic;

namespace WebApi.Models
{
    public class UserMasterRepository : IDisposable
    {
        public ResponseWrapper<MstUserEntity> objResponseWrapper = null;
        //public ResponseWrapper<UserSuperEntity> objAdminResponseWrapper = null;
        // SECURITY_DBEntities it is your context class
        StoreManagementEntities context = new StoreManagementEntities();
        //This method is used to check and validate the user credentials
        //public ResponseWrapper<UserEntity> ValidateUser(string username, string password)
        //{
        //    var users =  context.MstUsers.FirstOrDefault(user =>
        //    user.Email.Equals(username, StringComparison.OrdinalIgnoreCase)
        //    && user.Password == password);
        //    //UserEntity userEntity = new UserEntity();
        //    if (users != null)
        //    {
        //        Mapper.CreateMap<MstUser, UserEntity>()
        //             .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.MstRole))
        //             .ForMember(dest => dest.Companys, opt => opt.MapFrom(src => src.MstCompany))
        //             .ForMember(dest => dest.Groups, opt => opt.MapFrom(src => src.GroupDetail));
        //        Mapper.CreateMap<MstRole, RoleEntity>()
        //            .ForMember(dest => dest.GroupDetail, opt => opt.MapFrom(src => src.GroupDetail));
        //        Mapper.CreateMap<MstCompany, CompanyEntity>()
        //            .ForMember(dest => dest.GroupDetail, opt => opt.MapFrom(src => src.GroupDetail));
        //        Mapper.CreateMap<GroupDetail, GroupDetailEntity>();
        //        var mstUserModel = Mapper.Map<MstUser, UserEntity>(users);
        //        List<UserEntity> lstUser = new List<UserEntity>();
        //        if (mstUserModel != null)
        //        {
        //            if (!mstUserModel.Active)
        //            {
        //                new ApiDataException(1001, "User is not active.", HttpStatusCode.NotFound);
        //                return objResponseWrapper = new ResponseWrapper<UserEntity>(null, HttpStatusCode.NoContent, "User is not active.", 1);
        //            }
        //            lstUser.Add(mstUserModel);
        //            return objResponseWrapper = new ResponseWrapper<UserEntity>(lstUser, HttpStatusCode.OK, null, 0);
        //        }
        //        else
        //        {
        //            new ApiDataException(1001, "No user found for this id.", HttpStatusCode.NotFound);
        //            return objResponseWrapper = new ResponseWrapper<UserEntity>(null, HttpStatusCode.OK, "No user found for this id.", 1);
        //        }
        //    }
        //    return null;
        //}
        public ResponseWrapper<MstUserEntity> ValidateUser(string username, string password)
        {
            var users = context.MstUsers.FirstOrDefault(user =>
           user.UserName.Equals(username, StringComparison.OrdinalIgnoreCase)
           && user.Password == password);
            //UserEntity userEntity = new UserEntity();
            if (users != null)
            {
                MstUserEntity userEntity = MappingObject(users);

                List<MstUserEntity> lstUser = new List<MstUserEntity>();
                if (userEntity != null)
                {
                   
                    //if (!userEntity.Approved)
                    //{
                    //    //new ApiDataException(1001, "User is not active.", HttpStatusCode.NotFound);
                    //    return objResponseWrapper = new ResponseWrapper<UserEntity>(null, HttpStatusCode.NoContent, "Your account is not active please acitve using code sent on email.", 1);
                    //}
                    lstUser.Add(userEntity);
                    return objResponseWrapper = new ResponseWrapper<MstUserEntity>(lstUser, HttpStatusCode.OK, null, 0);
                }
                else
                {
                    //new ApiDataException(1001, "No user found for this id.", HttpStatusCode.NotFound);
                    return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "User name or password is wrong.", 1);
                }
            }
            return objResponseWrapper = new ResponseWrapper<MstUserEntity>(null, HttpStatusCode.OK, "User name or password is wrong.", 1);
        }

        //public ResponseWrapper<UserSuperEntity> ValidateAdminUser(string username, string password)
        //{
        //    string pwd = WebUtility.UrlDecode(password);
        //    var users = context.Users.FirstOrDefault(user =>
        //   user.Email.Equals(username, StringComparison.OrdinalIgnoreCase)
        //   && user.Password == pwd);
        //    //UserEntity userEntity = new UserEntity();
        //    if (users != null)
        //    {
        //        Mapper.CreateMap<User, UserSuperEntity>();
        //        var mstUserModel = Mapper.Map<User, UserSuperEntity>(users);
        //        List<UserSuperEntity> lstUser = new List<UserSuperEntity>();
        //        if (mstUserModel != null)
        //        {
        //            lstUser.Add(mstUserModel);
        //            return objAdminResponseWrapper = new ResponseWrapper<UserSuperEntity>(lstUser, HttpStatusCode.OK, null, 0);
        //        }
        //        else
        //        {
        //            //new ApiDataException(1001, "User name or password it not wrong.", HttpStatusCode.NotFound);
        //            return objAdminResponseWrapper = new ResponseWrapper<UserSuperEntity>(null, HttpStatusCode.OK, "User name or password is wrong.", 1);
        //        }
        //    }
        //    return objAdminResponseWrapper = new ResponseWrapper<UserSuperEntity>(null, HttpStatusCode.OK, "User name or password is wrong.", 1);
        //}
        public void Dispose()
        {
            context.Dispose();
        }

        public MstUserEntity MappingObject(MstUser item)
        {
            MstUserEntity objUser = new MstUserEntity();
            objUser.UserName = item.UserName;
            objUser.FirstName = item.FirstName;
            objUser.LastName = item.LastName;
            objUser.DOB = item.DOB;
            objUser.Lock = item.Lock;
            objUser.Password = item.Password;
            objUser.Gender = item.Gender;
            objUser.RoleId = item.RoleId;
            objUser.Password = item.Password;
            objUser.ModifiedBy = item.ModifiedBy;
            objUser.ModifiedDate = item.ModifiedDate;

            MstRole mstRole = context.MstRoles.Where(x=>x.RoleId ==objUser.RoleId).FirstOrDefault();
            MstRoleEntity objroleEntity = new MstRoleEntity();
            objroleEntity.RoleId = mstRole.RoleId;
            objroleEntity.RoleName = mstRole.RoleName;
          
            objUser.MstRole = objroleEntity;
            return objUser;
        }
    }

}