﻿using System.ComponentModel.Composition;
using DataModel;
using DataModel.UnitOfWork;
using Resolver;

namespace BusinessServices
{
    [Export(typeof(IComponent))]
    public class DependencyResolver : IComponent
    {
        public void SetUp(IRegisterComponent registerComponent)
        {   
            registerComponent.RegisterType<IMstUserServices, MstUserServices>();
            registerComponent.RegisterType<IMstRoleServices, MstRoleServices>();
            registerComponent.RegisterType<IMstSettingServices, MstSettingServices>();
            registerComponent.RegisterType<IMstProductServices, MstProductServices>();
            registerComponent.RegisterType<IMstDistributorServices, MstDistributorServices>();
            registerComponent.RegisterType<IInventoryServices, InventoryServices>();
            registerComponent.RegisterType<IMstCustomerServices, MstCustomerServices>();
            registerComponent.RegisterType<ISaleOrderServices, SaleOrderServices>();
            registerComponent.RegisterType<IMenuMasterServices, MenuMasterServices>();
            registerComponent.RegisterType<IRoleMenuServices, RoleMenuServices>();
        }
    }
}
