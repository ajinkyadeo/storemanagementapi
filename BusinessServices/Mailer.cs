﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Net.Mail;
//using System.ComponentModel;
using System.Threading;

namespace BusinessServices
{
    public class Mailer
    {
        //private BackgroundWorker _backgroundWorker;
        Thread emailWorker = null;
        string _strTo = string.Empty;
        string _strFrm = string.Empty;
        string _strCC = string.Empty;
        string _strSubject = string.Empty;
        string _strBody = string.Empty;
        string _FilePath = string.Empty;

        public Mailer()
        {
            //CREATE NEW BACKGROUND WORKER AND ADD EVENT HANDLERS
            //_backgroundWorker = new BackgroundWorker();
            //_backgroundWorker.WorkerReportsProgress = true;
            //_backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            //_backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
            //_backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
        }

        public bool SendEmail(string strTo, string strFrm, string strCC, string strSubject, string strBody, string FilePath)
        {
            try
            {
                //BackgroundWorker.
                _strTo = strTo;
                _strFrm = strFrm;
                _strCC = strCC;
                _strSubject = strSubject;
                _strBody = strBody;
                _FilePath = FilePath;
                try
                {
                    if (emailWorker == null)
                        emailWorker = new Thread(new ThreadStart(SendEmail_Background));

                    emailWorker.IsBackground = true;
                    emailWorker.Name = "SendEmailBackgroundWorker";
                    emailWorker.Start();
                }
                catch
                {
                    if (emailWorker != null)
                    {
                        if (emailWorker.IsAlive)
                            emailWorker.Abort();
                        emailWorker = null;
                    }
                }

                //_backgroundWorker.RunWorkerAsync();
                return true;
                //return SendEmail_Background(strTo, strFrm, strCC, strSubject, strBody, FilePath);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public bool SendEmailUser(string strTo, string strFrm, string strCC, string strSubject, string strBody, string FilePath)
        {
            try
            {
                //BackgroundWorker.
                _strTo = strTo;
                _strFrm = strFrm;
                _strCC = strCC;
                _strSubject = strSubject;
                _strBody = strBody;
                _FilePath = FilePath;

                try
                {
                    if (emailWorker == null)
                        emailWorker = new Thread(new ThreadStart(SendEmailUser_Background));

                    emailWorker.IsBackground = true;
                    emailWorker.Name = "SendEmailUserBackgroundWorker";
                    emailWorker.Start();
                }
                catch
                {
                    if (emailWorker != null)
                    {
                        if (emailWorker.IsAlive)
                            emailWorker.Abort();
                        emailWorker = null;
                    }
                }

                //_backgroundWorker.RunWorkerAsync();
                return true;
                //return SendEmailUser_Background(strTo, strCC, strSubject, strBody, FilePath);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        private void SendEmailUser_Background()
        {
            try
            {
                string strFrom = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eMailFrom"]);
                string SMTPServerName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPAddress"]);
                string strSMTPUser = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPUser"]);
                string strSMTPPass = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPPass"]);

                //string strFrom = strFrm;

                System.Net.Mail.MailMessage objMM = new System.Net.Mail.MailMessage();
                Attachment FileAttachement = null;

                // configure your mail appearance 
                int NoOfRecipients;
                string[] ListOfMailIDs;
                NoOfRecipients = _strTo.Split(';').Length;
                ListOfMailIDs = _strTo.Split(';');

                for (int i = 0; i < NoOfRecipients; i++)
                {
                    objMM.To.Add(ListOfMailIDs[i].ToString());
                }
                if (!string.IsNullOrEmpty(_strCC))
                {
                    NoOfRecipients = _strCC.Split(';').Length;
                    ListOfMailIDs = _strCC.Split(';');

                    for (int i = 0; i < NoOfRecipients; i++)
                    {
                        objMM.CC.Add(ListOfMailIDs[i].ToString());
                    }
                    //objMM.CC.Add(strFrom);
                }


                //objMM.CC.Add(strFrom);

                //objMM.CC.Add(strCC);
                objMM.From = new System.Net.Mail.MailAddress(strFrom);
                objMM.IsBodyHtml = true;
                objMM.Subject = _strSubject;
                objMM.Body = _strBody;
                objMM.Priority = MailPriority.High;

                if (!string.IsNullOrEmpty(_FilePath))
                {
                    FileAttachement = new Attachment(_FilePath);
                    objMM.Attachments.Add(FileAttachement);
                }

                // define smtp and authentication credential 

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                smtp.Host = SMTPServerName;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPort"]);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                smtp.Timeout = 10000;
                if (!string.IsNullOrEmpty(strSMTPUser))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(strSMTPUser, strSMTPPass);
                }

                // send the mail now 
                smtp.Send(objMM);
                //IsMailSent = true;
                //' Clear the attchment object and file name 
                objMM.Attachments.Clear();
                //FileName = null;

                //FileAttachement = null;
                //return IsMailSent;
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        private void SendEmail_Background()
        {
            try
            {
                string strFrom = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["eMailFrom"]);
                string SMTPServerName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPAddress"]);
                string strSMTPUser = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPUser"]);
                string strSMTPPass = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SMTPPass"]);

                //string strFrom = strFrm;

                System.Net.Mail.MailMessage objMM = new System.Net.Mail.MailMessage();
                Attachment FileAttachement = null;

                // configure your mail appearance 
                int NoOfRecipients;
                string[] ListOfMailIDs;
                NoOfRecipients = _strTo.Split(';').Length;
                ListOfMailIDs = _strTo.Split(';');

                for (int i = 0; i < NoOfRecipients; i++)
                {
                    objMM.To.Add(ListOfMailIDs[i].ToString());
                }
                if (!string.IsNullOrEmpty(_strCC))
                {
                    NoOfRecipients = _strCC.Split(';').Length;
                    ListOfMailIDs = _strCC.Split(';');

                    for (int i = 0; i < NoOfRecipients; i++)
                    {
                        objMM.CC.Add(ListOfMailIDs[i].ToString());
                    }
                }


                //objMM.CC.Add(_strFrm);

                //objMM.CC.Add(strCC);
                objMM.From = new System.Net.Mail.MailAddress(strFrom);
                objMM.IsBodyHtml = true;
                objMM.Subject = _strSubject;
                objMM.Body = _strBody;
                objMM.Priority = MailPriority.High;

                if (!string.IsNullOrEmpty(_FilePath))
                {
                    FileAttachement = new Attachment(_FilePath);
                    objMM.Attachments.Add(FileAttachement);
                }

                // define smtp and authentication credential 

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

                smtp.Host = SMTPServerName;
                smtp.Port = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTPPass"]);
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                if (!string.IsNullOrEmpty(strSMTPUser))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(strSMTPUser, strSMTPPass);
                }

                // send the mail now 
                smtp.Send(objMM);
                //IsMailSent = true;
                //' Clear the attchment object and file name 
                objMM.Attachments.Clear();
                //FileName = null;

                //FileAttachement = null;
                //return IsMailSent;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///// <summary>
        ///// Handles the backgroundWorker DoWork event
        ///// </summary>
        ///// <param name="sender">object</param>
        ///// <param name="e">DoWorkEventArgs</param>
        //private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        if (isSendEmailUser)
        //        {
        //            SendEmailUser_Background(_strTo, _strCC, _strSubject, _strBody, _FilePath);
        //        }
        //        else
        //        {
        //            SendEmail_Background(_strTo, _strFrm, _strCC, _strSubject, _strBody, _FilePath);
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        throw Ex;
        //    }
        //}

        ///// <summary>
        ///// Handles the backgroundWorker Worker Completed event
        ///// </summary>
        ///// <param name="sender">object</param>
        ///// <param name="e">RunWorkerCompletedEventArgs</param>
        //private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    //CHECK THE RESULT STATUS
        //    if (Convert.ToBoolean(e.Result))
        //    {
        //        #region ON SUCCESS

        //        #endregion
        //    }
        //    else
        //    {
        //        #region ON FAILURE

        //        #endregion
        //    }
        //}

        ///// <summary>
        ///// Handles the backgroundWorker Progress Changed event
        ///// </summary>
        ///// <param name="sender">object</param>
        ///// <param name="e">ProgressChangedEventArgs</param>
        //private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{

        //}

        public bool IsBodyHtml { get; set; }

        public bool IsBodyHTML { get; set; }
    }
}
