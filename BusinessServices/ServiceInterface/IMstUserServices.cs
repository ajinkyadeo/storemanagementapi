﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IMstUserServices
    {
        IEnumerable<MstUserEntity> GetAllUsers();
        MstUserEntity CreateUser(MstUserEntity userEntity);
        MstUserEntity GetByUserId(int userId);
        bool UpdateUser(MstUserEntity userEntity);
        MstUserEntity GetUserByUserNamePassword(string userName, string password);
        bool IsUserExist(string username);
        bool UpdateProfile(MstUserEntity userEntity);
        bool UpdatePassword(MstUserEntity userEntity);
        bool LockUser(int userId,bool isLock);
        bool GetDeleteUser(int id);

    }
}
