﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IMstProductServices
    {
        MstProductEntity GetById(int id);
        IEnumerable<MstProductEntity> GetAllProduct();
        IEnumerable<MstProductEntity> GetAllProductByInventory();

    }
}
