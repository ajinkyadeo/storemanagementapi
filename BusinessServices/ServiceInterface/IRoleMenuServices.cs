﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// RoleMenu Service Contract
    /// </summary>
    public interface IRoleMenuServices
    {
        IEnumerable<RoleMenuEntity> GetAllRoleMenus();
        RoleMenuEntity CreateRoleMenu(RoleMenuEntity RoleMenuEntity);
        RoleMenuEntity GetByRoleMenuId(int RoleMenuId);
        bool UpdateRoleMenu(RoleMenuEntity RoleMenuEntity);
        bool GetDeleteRoleMenu(int id);
        bool CreateBulkRoleMenu(List<RoleMenuEntity> roleMenuEntity);

        List<RoleMenuEntity> GetRoleMenuByRoleId(int RoleMenuId);

    }
}
