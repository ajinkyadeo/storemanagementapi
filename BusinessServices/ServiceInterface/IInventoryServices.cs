﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IInventoryServices
    {
        IEnumerable<InventoryEntity> GetAllInventorys();
        InventoryEntity CreateInventory(InventoryEntity inventoryEntity);
        InventoryEntity GetByInventoryId(int id);
        bool UpdateInventory(InventoryEntity inventoryEntity);
        bool GetDeleteInventory(int id);
        InventoryEntity GetByProductId(int productId);

        List<InventoryEntity> GetAllInventorysByProductId(int productId, int distId);


    }
}
