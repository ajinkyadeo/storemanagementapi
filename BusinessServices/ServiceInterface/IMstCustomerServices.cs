﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// Customer Service Contract
    /// </summary>
    public interface IMstCustomerServices
    {
        IEnumerable<MstCustomerEntity> GetAllCustomers();
        MstCustomerEntity CreateCustomer(MstCustomerEntity CustomerEntity);
        MstCustomerEntity GetByCustomerId(int CustomerId);
        bool UpdateCustomer(MstCustomerEntity CustomerEntity);
        bool GetDeleteCustomer(int id);

    }
}
