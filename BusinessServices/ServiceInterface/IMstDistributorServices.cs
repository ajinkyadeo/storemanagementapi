﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IMstDistributorServices
    {
        MstDistributorEntity GetById(int id);
        IEnumerable<MstDistributorEntity> GetAllDistributor();

        IEnumerable<MstDistributorEntity> GetAllDistributorByProduct(int productId);

    }
}
