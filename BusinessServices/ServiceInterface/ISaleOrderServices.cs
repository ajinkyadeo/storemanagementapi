﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface ISaleOrderServices
    {
        IEnumerable<SaleOrderEntity> GetAllSaleOrders();
        SaleOrderEntity CreateSaleOrder(SaleOrderEntity SaleOrderEntity);
        SaleOrderEntity GetBySaleOrderId(int id);
        bool UpdateSaleOrder(SaleOrderEntity SaleOrderEntity);
        bool GetDeleteSaleOrder(int id);

    }
}
