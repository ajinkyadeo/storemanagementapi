﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IMstRoleServices
    {
        MstRoleEntity GetById(int id);
        IEnumerable<MstRoleEntity> GetAllRole();

    }
}
