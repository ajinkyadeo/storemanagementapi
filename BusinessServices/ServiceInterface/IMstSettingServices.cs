﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// User Service Contract
    /// </summary>
    public interface IMstSettingServices
    {
        MstSettingEntity GetById(int id);
        IEnumerable<MstSettingEntity> GetAllSetting();
        bool UpdateSetting(MstSettingEntity settingEntity);

    }
}
