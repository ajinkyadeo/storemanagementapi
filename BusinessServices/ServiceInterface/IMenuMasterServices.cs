﻿using System.Collections.Generic;
using BusinessEntities;

namespace BusinessServices
{
    /// <summary>
    /// MenuMaster Service Contract
    /// </summary>
    public interface IMenuMasterServices
    {
        IEnumerable<MenuMasterEntity> GetAllMenuMasters();
        MenuMasterEntity CreateMenuMaster(MenuMasterEntity MenuMasterEntity);
        MenuMasterEntity GetByMenuMasterId(int MenuMasterId);
        bool UpdateMenuMaster(MenuMasterEntity MenuMasterEntity);
        bool GetDeleteMenuMaster(int id);
        List<MenuMasterEntity> GetMenuByRoleId(int roleId);
    }
}
