﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstSettingServices : IMstSettingServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstSettingServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public BusinessEntities.MstSettingEntity GetById(int id)
        {
            var property = _unitOfWork.MstSettingRepository.GetByID(id);
            if (property != null)
            {
                return MappingSingleObject(property);
            }
            return new MstSettingEntity();
        }

        public IEnumerable<MstSettingEntity> GetAllSetting()
        {
            List<MstSettingEntity> lstUser = new List<MstSettingEntity>();
            var property = _unitOfWork.MstSettingRepository.GetAll().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.Id);
                return lstUser;
            }
            return lstUser;
        }
        public bool UpdateSetting(MstSettingEntity SettingEntity)
        {
            var success = false;
            if (SettingEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.MstSettingRepository.GetByID(SettingEntity.Id);
                    if (user != null)
                    {
                        user.Name = SettingEntity.Name;
                        user.Value = SettingEntity.Value;
                       
                        _unitOfWork.MstSettingRepository.Update(user);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public List<MstSettingEntity> Mapping(List<MstSetting> prop)
        {
            List<MstSettingEntity> lstSetting = new List<MstSettingEntity>();
            foreach (var item in prop)
            {
                MstSettingEntity objSetting = new MstSettingEntity();
                objSetting.Name = item.Name;
                objSetting.Value = item.Value;
                objSetting.Id = item.Id;

                lstSetting.Add(objSetting);
            }
            return lstSetting;
        }

        public MstSettingEntity MappingSingleObject(MstSetting item)
        {
            MstSettingEntity objSetting = new MstSettingEntity();
            objSetting.Name = item.Name;
            objSetting.Value = item.Value;
            objSetting.Id = item.Id;

            return objSetting;
        }
    }
}
