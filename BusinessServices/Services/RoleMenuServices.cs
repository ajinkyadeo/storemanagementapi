﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class RoleMenuServices : IRoleMenuServices
    {

        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();
        //private object _unitofwork;

        public RoleMenuServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public RoleMenuEntity GetByRoleMenuId(int RoleMenuId)
        {
            var RoleMenus = _unitOfWork.RoleMenuRepository.GetByID(RoleMenuId);
            if (RoleMenus != null)
            {
                return MappingSingleObject(RoleMenus);
            }
            return new RoleMenuEntity();
        }

        public IEnumerable<RoleMenuEntity> GetAllRoleMenus()
        {
            var RoleMenus = _unitOfWork.RoleMenuRepository.GetAll().ToList();
            if (RoleMenus.Any())
            {
                List<RoleMenuEntity> lstRoleMenu =  Mapping(RoleMenus);
                if (lstRoleMenu.Count > 0)
                    return lstRoleMenu.OrderByDescending(x => x.Id).ToList();
                return lstRoleMenu;
            }
            return new List<RoleMenuEntity>();
        }

        public RoleMenuEntity CreateRoleMenu(RoleMenuEntity RoleMenuEntity)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var RoleMenu = new RoleMenu
                    {

                        RoleId = RoleMenuEntity.RoleId,
                        MenuId = RoleMenuEntity.MenuId,
                    
                    };
                    _unitOfWork.RoleMenuRepository.Insert(RoleMenu);
                    _unitOfWork.Save();
                  
                    var RoleMenus = _unitOfWork.RoleMenuRepository.GetByID(RoleMenu.Id);
                    RoleMenuEntity RoleMenuModel = MappingSingleObject(RoleMenus);
                    scope.Complete();
                    return RoleMenuModel;
                }
            }
            catch (Exception es)
            {
                return null;
            }

        }

        public bool UpdateRoleMenu(RoleMenuEntity RoleMenuEntity)
        {
            try
            {
                var success = false;
                if (RoleMenuEntity != null)
                {
                    using (var scope = new TransactionScope())
                    {
                        var RoleMenu = _unitOfWork.RoleMenuRepository.GetByID(RoleMenuEntity.Id);
                        if (RoleMenu != null)
                        {

                            RoleMenu.Id = RoleMenuEntity.Id;
                            RoleMenu.RoleId = RoleMenuEntity.RoleId;
                            RoleMenu.MenuId = RoleMenuEntity.MenuId;
                         
                            _unitOfWork.RoleMenuRepository.Update(RoleMenu);
                            _unitOfWork.Save();
                            scope.Complete();
                            success = true;
                        }
                    }
                }
                return success;
            }
            catch (Exception es)
            {
                return false;
            }

        }
        public bool GetDeleteRoleMenu(int id)
    {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {
                
                _unitOfWork.RoleMenuRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }

        public bool CreateBulkRoleMenu(List<RoleMenuEntity> RoleMenuEntity)
        {
            bool success = false;
            using (var scope = new TransactionScope())
            {
                var roleMenus = _unitOfWork.RoleMenuRepository.GetAll().Where(x => x.RoleId == RoleMenuEntity[0].RoleId);
                foreach (var items in roleMenus)
                {
                   _unitOfWork.RoleMenuRepository.Delete(items);
                }
                foreach (var RoleMenuEntitys in RoleMenuEntity)
                {


                    var property = new RoleMenu
                    {
                        RoleId = RoleMenuEntitys.RoleId,
                        MenuId = RoleMenuEntitys.MenuId,
                    };
                    _unitOfWork.RoleMenuRepository.Insert(property);
                    _unitOfWork.Save();
                    //TrnWorkshopChecklistEntity TrnWorkshopChecklistModel = MappingSingleObject(property);

                    success = true;
                }
                scope.Complete();
                return success;
            }

        }


        public List<RoleMenuEntity> GetRoleMenuByRoleId(int roleId)
        {
            var RoleMenus = _context.RoleMenus.Where(x => x.RoleId == roleId).ToList();
            if (RoleMenus != null)
            {
                return Mapping(RoleMenus);
            }
            return new List<RoleMenuEntity>();
        }

        public List<RoleMenuEntity> Mapping(List<RoleMenu> roleMenu)
        {
            List<RoleMenuEntity> lstRoleMenu = new List<RoleMenuEntity>();
            foreach (var item in roleMenu)
            {
                RoleMenuEntity objRoleMenu = new RoleMenuEntity();
                objRoleMenu.Id = item.Id;
                objRoleMenu.RoleId = item.RoleId;
                objRoleMenu.MenuId = item.MenuId;
                objRoleMenu.AddPermission = item.AddPermission;
                objRoleMenu.EditPermission = item.EditPermission;
                objRoleMenu.DeletePermission = item.DeletePermission;
                objRoleMenu.ViewPermission = item.ViewPermission;

                MenuMasterEntity objMenuMaster = new MenuMasterEntity();
                objMenuMaster.Id = item.MenuMaster.Id;
                objMenuMaster.MenuName = item.MenuMaster.MenuName;
                objMenuMaster.RouterLink = item.MenuMaster.RouterLink;
                objMenuMaster.Icon = item.MenuMaster.Icon;
                objMenuMaster.Class = item.MenuMaster.Class;
                objMenuMaster.MenuOrder = item.MenuMaster.MenuOrder;

                objRoleMenu.menus = objMenuMaster;

                lstRoleMenu.Add(objRoleMenu);
            }
            return lstRoleMenu;
        }

        public RoleMenuEntity MappingSingleObject(RoleMenu item)
        {
            RoleMenuEntity objRoleMenu = new RoleMenuEntity();
            objRoleMenu.Id = item.Id;
            objRoleMenu.RoleId = item.RoleId;
            objRoleMenu.MenuId = item.MenuId;
            objRoleMenu.AddPermission = item.AddPermission;
            objRoleMenu.EditPermission = item.EditPermission;
            objRoleMenu.DeletePermission = item.DeletePermission;
            objRoleMenu.ViewPermission = item.ViewPermission;

            MenuMasterEntity objMenuMaster = new MenuMasterEntity();
            objMenuMaster.Id = item.MenuMaster.Id;
            objMenuMaster.MenuName = item.MenuMaster.MenuName;
            objMenuMaster.RouterLink = item.MenuMaster.RouterLink;
            objMenuMaster.Icon = item.MenuMaster.Icon;
            objMenuMaster.Class = item.MenuMaster.Class;
            objMenuMaster.MenuOrder = item.MenuMaster.MenuOrder;

            objRoleMenu.menus = objMenuMaster;

            return objRoleMenu;
        }
    }
}
