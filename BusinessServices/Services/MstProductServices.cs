﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstProductServices : IMstProductServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstProductServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public BusinessEntities.MstProductEntity GetById(int id)
        {
            var property = _unitOfWork.MstProductRepository.GetByID(id);
            if (property != null)
            {
                return MappingSingleObject(property);
            }
            return new MstProductEntity();
        }

        public IEnumerable<MstProductEntity> GetAllProduct()
        {
            List<MstProductEntity> lstUser = new List<MstProductEntity>();
            var property = _unitOfWork.MstProductRepository.GetAll().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.ProductId);
                return lstUser;
            }
            return lstUser;
        }

        public IEnumerable<MstProductEntity> GetAllProductByInventory()
        {
            List<MstProductEntity> lstUser = new List<MstProductEntity>();
            var property = (from p in _context.MstProducts
                            join ip in _context.Inventories on p.ProductId equals ip.ProductId
                            where ip.Units > 0
                            select p
                    ).Distinct().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.ProductId);
                return lstUser;
            }
            return lstUser;
        }

        public List<MstProductEntity> Mapping(List<MstProduct> prop)
        {
            List<MstProductEntity> lstRole = new List<MstProductEntity>();
            foreach (var item in prop)
            {
                MstProductEntity objRole = new MstProductEntity();
                objRole.ProductId = item.ProductId;
                objRole.ProductName = item.ProductName;
                objRole.Manufacturer = item.Manufacturer;
                objRole.MRP = item.MRP;


                lstRole.Add(objRole);
            }
            return lstRole;
        }

        public MstProductEntity MappingSingleObject(MstProduct item)
        {
            MstProductEntity objRole = new MstProductEntity();
            objRole.ProductId = item.ProductId;
            objRole.ProductName = item.ProductName;
            objRole.Manufacturer = item.Manufacturer;
            objRole.MRP = item.MRP;

            return objRole;
        }
    }
}
