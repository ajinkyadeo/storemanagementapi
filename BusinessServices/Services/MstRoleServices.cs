﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstRoleServices : IMstRoleServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstRoleServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public BusinessEntities.MstRoleEntity GetById(int id)
        {
            var property = _unitOfWork.MstRoleRepository.GetByID(id);
            if (property != null)
            {
                return MappingSingleObject(property);
            }
            return new MstRoleEntity();
        }

        public IEnumerable<MstRoleEntity> GetAllRole()
        {
            List<MstRoleEntity> lstUser = new List<MstRoleEntity>();
            var property = _unitOfWork.MstRoleRepository.GetAll().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.RoleName);
                return lstUser;
            }
            return lstUser;
        }
    
        public List<MstRoleEntity> Mapping(List<MstRole> prop)
        {
            List<MstRoleEntity> lstRole = new List<MstRoleEntity>();
            foreach (var item in prop)
            {
                MstRoleEntity objRole = new MstRoleEntity();
                objRole.RoleName = item.RoleName;
                objRole.RoleId = item.RoleId;

                lstRole.Add(objRole);
            }
            return lstRole;
        }

        public MstRoleEntity MappingSingleObject(MstRole item)
        {
            MstRoleEntity objRole = new MstRoleEntity();
            objRole.RoleName = item.RoleName;
            objRole.RoleId = item.RoleId;
            return objRole;
        }
    }
}
