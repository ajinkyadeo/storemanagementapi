﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class SaleOrderServices : ISaleOrderServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public SaleOrderServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public SaleOrderEntity GetBySaleOrderId(int SaleOrderId)
        {
            var SaleOrders = _unitOfWork.SaleOrderRepository.GetByID(SaleOrderId);
            if (SaleOrders != null)
            {
                return MappingSingleObject(SaleOrders);
            }
            return new SaleOrderEntity();
        }

        public IEnumerable<SaleOrderEntity> GetAllSaleOrders()
        {
            var SaleOrders = _unitOfWork.SaleOrderRepository.GetAll().ToList();
            if (SaleOrders.Any())
            {
                List<SaleOrderEntity> lstSaleOrder =  Mapping(SaleOrders);
                if (lstSaleOrder.Count > 0)
                    return lstSaleOrder.OrderByDescending(x => x.Id).ToList();
                return lstSaleOrder;
            }
            return new List<SaleOrderEntity>();
        }

        public SaleOrderEntity CreateSaleOrder(SaleOrderEntity SaleOrderEntity)
        {
            using (var scope = new TransactionScope())
            {
                var SaleOrder = new SaleOrder
                {
                    //Id = SaleOrderEntity.Id,
                    ProductId = SaleOrderEntity.ProductId,
                    DistributorId = SaleOrderEntity.DistributorId,
                    DiscountPer = SaleOrderEntity.DiscountPer,                       
                    DiscountVal = SaleOrderEntity.DiscountVal,
                    MRP = SaleOrderEntity.MRP,
                    Price = SaleOrderEntity.Price,
                    CustomerId = SaleOrderEntity.CustomerId,
                };
                _unitOfWork.SaleOrderRepository.Insert(SaleOrder);
                _unitOfWork.Save();

                var inventory = _unitOfWork.InventoryRepository.GetAll().Where(x => x.ProductId == SaleOrderEntity.ProductId && x.DistributorId == SaleOrderEntity.DistributorId).FirstOrDefault();

                if(inventory != null)
                {
                    inventory.Units = inventory.Units - 1;

                    _unitOfWork.InventoryRepository.Update(inventory);
                    _unitOfWork.Save();

                }

                var SaleOrders = _unitOfWork.SaleOrderRepository.GetByID(SaleOrder.Id);
                SaleOrderEntity SaleOrderModel = MappingSingleObject(SaleOrders);
                scope.Complete();
                return SaleOrderModel;
            }
        }
        public bool GetDeleteSaleOrder(int id)
        {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {
                var saleorder = _unitOfWork.SaleOrderRepository.GetByID(id);

                var inventory = _unitOfWork.InventoryRepository.GetAll().Where(x => x.ProductId == saleorder.ProductId && x.DistributorId == saleorder.DistributorId).FirstOrDefault();

                if (inventory != null)
                {
                    inventory.Units = inventory.Units + 1;

                    _unitOfWork.InventoryRepository.Update(inventory);
                    _unitOfWork.Save();

                }


                _unitOfWork.SaleOrderRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }

        public bool UpdateSaleOrder(SaleOrderEntity SaleOrderEntity)
        {
            var success = false;
            if (SaleOrderEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var SaleOrder = _unitOfWork.SaleOrderRepository.GetByID(SaleOrderEntity.Id);
                    if (SaleOrder != null)
                    {
                        SaleOrder.Id = SaleOrderEntity.Id;
                        SaleOrder.ProductId = SaleOrderEntity.ProductId;
                        SaleOrder.DistributorId = SaleOrderEntity.DistributorId;
                        SaleOrder.DiscountPer = SaleOrderEntity.DiscountPer;
                        SaleOrder.DiscountVal = SaleOrderEntity.DiscountVal;
                        SaleOrder.MRP = SaleOrderEntity.MRP;
                        SaleOrder.Price = SaleOrderEntity.Price;
                        SaleOrder.CustomerId = SaleOrderEntity.CustomerId;

                        _unitOfWork.SaleOrderRepository.Update(SaleOrder);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;


                    }
                }
            }
            return success;
        }
        public List<SaleOrderEntity> Mapping(List<SaleOrder> deal)
        {
            List<SaleOrderEntity> lstSaleOrder = new List<SaleOrderEntity>();
            foreach (var item in deal)
            {
                SaleOrderEntity objSaleOrder = new SaleOrderEntity();
                objSaleOrder.Id = item.Id;
                objSaleOrder.DistributorId = item.DistributorId;
                objSaleOrder.ProductId = item.ProductId;
                objSaleOrder.DiscountVal = item.DiscountVal;
                objSaleOrder.DiscountPer = item.DiscountPer;
                objSaleOrder.MRP = item.MRP;
                objSaleOrder.Price = item.Price;
                objSaleOrder.CustomerId = item.CustomerId;

                MstProduct mstProduct = _unitOfWork.MstProductRepository.GetByID(objSaleOrder.ProductId);
                MstProductEntity objProduct = new MstProductEntity();
                objProduct.ProductId = mstProduct.ProductId;
                objProduct.ProductName = mstProduct.ProductName;
                objProduct.Manufacturer = mstProduct.Manufacturer;
                objProduct.MRP = mstProduct.MRP;

                objSaleOrder.MstProduct = objProduct;

                MstDistributor mstDistributor = _unitOfWork.MstDistributorRepository.GetByID(objSaleOrder.DistributorId);
                MstDistributorEntity objDistributor = new MstDistributorEntity();
                objDistributor.DistributorName = mstDistributor.DistributorName;
                objDistributor.DistributorId = mstDistributor.DistributorId;
                objDistributor.Code = mstDistributor.Code;
                objDistributor.Address = mstDistributor.Address;
                objDistributor.PhoneNumber = mstDistributor.PhoneNumber;
                objDistributor.ContactPerson = mstDistributor.ContactPerson;
                objDistributor.CreatedBy = mstDistributor.CreatedBy;
                objDistributor.CreatedDate = mstDistributor.CreatedDate;
                objDistributor.ModifiedBy = mstDistributor.ModifiedBy;
                objDistributor.ModifiedDate = mstDistributor.ModifiedDate;

                objSaleOrder.MstDistributor = objDistributor;


                MstCustomer mstCustomer = _unitOfWork.MstCustomerRepository.GetByID(objSaleOrder.CustomerId);
                MstCustomerEntity objCustomer = new MstCustomerEntity();
                objCustomer.CustomerId = mstCustomer.CustomerId;
                objCustomer.CustomerName = mstCustomer.CustomerName;
                objCustomer.PhoneNumber = mstCustomer.PhoneNumber;
                objCustomer.Email = mstCustomer.Email;

                objSaleOrder.MstCustomer = objCustomer;

                lstSaleOrder.Add(objSaleOrder);
            }
            return lstSaleOrder;
        }

        public SaleOrderEntity MappingSingleObject(SaleOrder item)
        {

            SaleOrderEntity objSaleOrder = new SaleOrderEntity();
            objSaleOrder.Id = item.Id;
            objSaleOrder.DistributorId = item.DistributorId;
            objSaleOrder.ProductId = item.ProductId;
            objSaleOrder.DiscountVal = item.DiscountVal;
            objSaleOrder.DiscountPer = item.DiscountPer;
            objSaleOrder.MRP = item.MRP;
            objSaleOrder.Price = item.Price;
            objSaleOrder.CustomerId = item.CustomerId;


            MstProduct mstProduct = _unitOfWork.MstProductRepository.GetByID(objSaleOrder.ProductId);
            MstProductEntity objProduct = new MstProductEntity();
            objProduct.ProductId = mstProduct.ProductId;
            objProduct.ProductName = mstProduct.ProductName;
            objProduct.Manufacturer = mstProduct.Manufacturer;
            objProduct.MRP = mstProduct.MRP;

            objSaleOrder.MstProduct = objProduct;

            MstDistributor mstDistributor = _unitOfWork.MstDistributorRepository.GetByID(objSaleOrder.DistributorId);
            MstDistributorEntity objDistributor = new MstDistributorEntity();
            objDistributor.DistributorName = mstDistributor.DistributorName;
            objDistributor.DistributorId = mstDistributor.DistributorId;
            objDistributor.Code = mstDistributor.Code;
            objDistributor.Address = mstDistributor.Address;
            objDistributor.PhoneNumber = mstDistributor.PhoneNumber;
            objDistributor.ContactPerson = mstDistributor.ContactPerson;
            objDistributor.CreatedBy = mstDistributor.CreatedBy;
            objDistributor.CreatedDate = mstDistributor.CreatedDate;
            objDistributor.ModifiedBy = mstDistributor.ModifiedBy;
            objDistributor.ModifiedDate = mstDistributor.ModifiedDate;

            objSaleOrder.MstDistributor = objDistributor;


            MstCustomer mstCustomer = _unitOfWork.MstCustomerRepository.GetByID(objSaleOrder.CustomerId);
            MstCustomerEntity objCustomer = new MstCustomerEntity();
            objCustomer.CustomerId = mstCustomer.CustomerId;
            objCustomer.CustomerName = mstCustomer.CustomerName;
            objCustomer.Email = mstCustomer.Email;
            objCustomer.PhoneNumber = mstCustomer.PhoneNumber;
            objSaleOrder.MstCustomer = objCustomer;

            return objSaleOrder;
        }
    }
}
