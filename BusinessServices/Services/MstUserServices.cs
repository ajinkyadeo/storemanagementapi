﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstUserServices : IMstUserServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstUserServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public MstUserEntity GetByUserId(int userId)
        {
            var users = _unitOfWork.MstUserRepository.GetByID(userId);
            if (users != null)
            {
                return MappingSingleObject(users);
            }
            return new MstUserEntity();
        }

        public IEnumerable<MstUserEntity> GetAllUsers()
        {
            var users = _unitOfWork.MstUserRepository.GetAll().ToList();
            if (users.Any())
            {
                List<MstUserEntity> lstUser =  Mapping(users);
                if (lstUser.Count > 0)
                    return lstUser.OrderByDescending(x => x.UserId).ToList();
                return lstUser;
            }
            return new List<MstUserEntity>();
        }

        public MstUserEntity CreateUser(MstUserEntity userEntity)
        {
            using (var scope = new TransactionScope())
            {
                var user = new MstUser
                {
                    UserName = userEntity.UserName,
                    FirstName = userEntity.FirstName,
                    LastName = userEntity.LastName,
                    DOB = userEntity.DOB,                       
                    Lock = userEntity.Lock,
                    Password = userEntity.Password,
                    PhoneNumber = userEntity.PhoneNumber,
                    Gender = userEntity.Gender,
                    RoleId = userEntity.RoleId,
                    CreatedBy = userEntity.CreatedBy,
                    CreatedDate = DateTime.Now,

                };
                _unitOfWork.MstUserRepository.Insert(user);
                _unitOfWork.Save();

                var users = _unitOfWork.MstUserRepository.GetByID(user.UserId);
                MstUserEntity mstUserModel = MappingSingleObject(users);
                scope.Complete();
                return mstUserModel;
            }
        }
        public bool GetDeleteUser(int id)
        {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {

                _unitOfWork.MstUserRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }

        public bool UpdateUser(MstUserEntity userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.MstUserRepository.GetByID(userEntity.UserId);
                    if (user != null)
                    {
                        user.UserName = userEntity.UserName;
                        user.FirstName = userEntity.FirstName;
                        user.LastName = userEntity.LastName;
                        user.DOB = userEntity.DOB;
                        user.Gender = userEntity.Gender;
                        user.Lock = userEntity.Lock;
                        user.PhoneNumber = userEntity.PhoneNumber;
                        user.RoleId = userEntity.RoleId;
                        user.ModifiedBy = userEntity.ModifiedBy;
                        user.ModifiedDate = DateTime.Now;

                        _unitOfWork.MstUserRepository.Update(user);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;


                    }
                }
            }
            return success;
        }
        public bool UpdateProfile(MstUserEntity userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.MstUserRepository.GetByID(userEntity.UserId);
                    if (user != null)
                    {
                        user.UserName = userEntity.UserName;
                        user.FirstName = userEntity.FirstName;
                        user.LastName = userEntity.LastName;
                        user.Gender = userEntity.Gender;
                        user.DOB = userEntity.DOB;
                        user.PhoneNumber = userEntity.PhoneNumber;
                        user.ModifiedBy = userEntity.ModifiedBy;
                        user.ModifiedDate = DateTime.Now;

                        _unitOfWork.MstUserRepository.Update(user);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public bool UpdatePassword(MstUserEntity userEntity)
        {
            var success = false;
            if (userEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.MstUserRepository.GetByID(userEntity.UserId);
                    if (user != null)
                    {
                        user.Password = userEntity.Password;
                        
                        user.ModifiedBy = userEntity.ModifiedBy;
                        user.ModifiedDate = DateTime.Now;

                        _unitOfWork.MstUserRepository.Update(user);
                        _unitOfWork.Save();


                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }

        public MstUserEntity GetUserByUserNamePassword(string userName, string password)
        {
            var user = _unitOfWork.MstUserRepository.GetAll().Where(u => u.UserName.ToLower().Trim() == userName.ToLower().Trim() && u.Password == password ).FirstOrDefault();
            if (user != null)
            {
                return MappingSingleObject(user);
            }
            return new MstUserEntity();
        }

        public bool IsUserExist(string username)
        {
            var user = _unitOfWork.MstUserRepository.GetAll().Where(u => u.UserName.ToLower().Trim() == username.ToLower().Trim()).FirstOrDefault();
            if (user != null)
            {
                return true;
            }
            return false;
        }

        public bool LockUser(int userId, bool isLock)
        {
            var success = false;
          
                using (var scope = new TransactionScope())
                {
                    var user = _unitOfWork.MstUserRepository.GetByID(userId);
                if (user != null)
                {
                    user.Lock = isLock?true:false;

                    _unitOfWork.MstUserRepository.Update(user);
                    _unitOfWork.Save();

                    scope.Complete();
                    success = true;
                }
            }
            return success;

        }
        public List<MstUserEntity> Mapping(List<MstUser> deal)
        {
            List<MstUserEntity> lstUser = new List<MstUserEntity>();
            foreach (var item in deal)
            {
                MstUserEntity objUser = new MstUserEntity();
                objUser.UserId = item.UserId;
                objUser.UserName = item.UserName;
                objUser.FirstName = item.FirstName;
                objUser.LastName = item.LastName;
                objUser.DOB = item.DOB;
                objUser.Password = item.Password;
                objUser.Gender = item.Gender;
                objUser.RoleId = item.RoleId;
                objUser.PhoneNumber = item.PhoneNumber;
                objUser.Password = item.Password;
                objUser.CreatedBy = item.CreatedBy;
                objUser.CreatedDate = item.CreatedDate;
                objUser.ModifiedBy = item.ModifiedBy;
                objUser.ModifiedDate = item.ModifiedDate;

                MstRole mstRole = _unitOfWork.MstRoleRepository.GetByID(objUser.RoleId);
                MstRoleEntity objroleEntity = new MstRoleEntity();
                objroleEntity.RoleId = mstRole.RoleId;
                objroleEntity.RoleName = mstRole.RoleName;
              
                objUser.MstRole = objroleEntity;

                lstUser.Add(objUser);
            }
            return lstUser;
        }

        public MstUserEntity MappingSingleObject(MstUser item)
        {

            MstUserEntity objUser = new MstUserEntity();
            objUser.UserId = item.UserId;
            objUser.UserName = item.UserName;
            objUser.FirstName = item.FirstName;
            objUser.LastName = item.LastName;
            objUser.DOB = item.DOB;
            objUser.Password = item.Password;
            objUser.Gender = item.Gender;
            objUser.RoleId = item.RoleId;
            objUser.PhoneNumber = item.PhoneNumber;
            objUser.Password = item.Password;
            objUser.CreatedBy = item.CreatedBy;
            objUser.CreatedDate = item.CreatedDate;
            objUser.ModifiedBy = item.ModifiedBy;
            objUser.ModifiedDate = item.ModifiedDate;

            MstRole mstRole = _unitOfWork.MstRoleRepository.GetByID(objUser.RoleId);
            MstRoleEntity objroleEntity = new MstRoleEntity();
            objroleEntity.RoleId = mstRole.RoleId;
            objroleEntity.RoleName = mstRole.RoleName;

            objUser.MstRole = objroleEntity;

            return objUser;
        }
    }
}
