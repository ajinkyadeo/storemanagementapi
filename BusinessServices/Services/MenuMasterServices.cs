﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MenuMasterServices : IMenuMasterServices
    {

        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();
        //private object _unitofwork;

        public MenuMasterServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public MenuMasterEntity GetByMenuMasterId(int MenuMasterId)
        {
            var MenuMasters = _unitOfWork.MenuMasterRepository.GetByID(MenuMasterId);
            if (MenuMasters != null)
            {
                return MappingSingleObject(MenuMasters);
            }
            return new MenuMasterEntity();
        }

        public IEnumerable<MenuMasterEntity> GetAllMenuMasters()
        {
            var MenuMasters = _unitOfWork.MenuMasterRepository.GetAll().ToList();
            if (MenuMasters.Any())
            {
                List<MenuMasterEntity> lstMenuMaster =  Mapping(MenuMasters);
                if (lstMenuMaster.Count > 0)
                    return lstMenuMaster.OrderByDescending(x => x.Id).ToList();
                return lstMenuMaster;
            }
            return new List<MenuMasterEntity>();
        }

        public List<MenuMasterEntity> GetMenuByRoleId(int roleId)
        {
            //var MenuMasters = _unitOfWork.MenuMasterRepository.GetAll().ToList();
            //if (MenuMasters.Any())
            //{
            //    List<MenuMasterEntity> lstMenuMaster = Mapping(MenuMasters);
            //    if (lstMenuMaster.Count > 0)
            //        return lstMenuMaster.OrderByDescending(x => x.Id).ToList();
            //    return lstMenuMaster;
            //}

            var menu = (from s in _context.MenuMasters // outer sequence
                            join st in _context.RoleMenus//inner sequence 
                            on s.Id equals st.MenuId
                            where st.RoleId == roleId
                            select s).Distinct().ToList();// key selector 
            List<MenuMasterEntity> lstMenuMaster = Mapping(menu);
            if (lstMenuMaster.Count > 0)
                return lstMenuMaster.OrderBy(x => x.MenuOrder).ToList();
            return lstMenuMaster;

        }

        public MenuMasterEntity CreateMenuMaster(MenuMasterEntity MenuMasterEntity)
        {
            try
            {
                using (var scope = new TransactionScope())
                {
                    var MenuMaster = new MenuMaster
                    {

                        MenuName = MenuMasterEntity.MenuName,
                        RouterLink = MenuMasterEntity.RouterLink,
                        Icon = MenuMasterEntity.Icon,
                        Class = MenuMasterEntity.Class,
                        MenuOrder = MenuMasterEntity.MenuOrder
                    };
                    _unitOfWork.MenuMasterRepository.Insert(MenuMaster);
                    _unitOfWork.Save();
                  
                    var MenuMasters = _unitOfWork.MenuMasterRepository.GetByID(MenuMaster.Id);
                    MenuMasterEntity MenuMasterModel = MappingSingleObject(MenuMasters);
                    scope.Complete();
                    return MenuMasterModel;
                }
            }
            catch (Exception es)
            {
                return null;
            }

        }

        public bool UpdateMenuMaster(MenuMasterEntity MenuMasterEntity)
        {
            try
            {
                var success = false;
                if (MenuMasterEntity != null)
                {
                    using (var scope = new TransactionScope())
                    {
                        var MenuMaster = _unitOfWork.MenuMasterRepository.GetByID(MenuMasterEntity.Id);
                        if (MenuMaster != null)
                        {

                            MenuMaster.Id = MenuMasterEntity.Id;
                            MenuMaster.MenuName = MenuMasterEntity.MenuName;
                            MenuMaster.RouterLink = MenuMasterEntity.RouterLink;
                            MenuMaster.Icon = MenuMasterEntity.Icon;
                            MenuMaster.Class = MenuMasterEntity.Class;
                            MenuMaster.MenuOrder = MenuMasterEntity.MenuOrder;
                                
                            _unitOfWork.MenuMasterRepository.Update(MenuMaster);
                            _unitOfWork.Save();
                            scope.Complete();
                            success = true;
                        }
                    }
                }
                return success;
            }
            catch (Exception es)
            {
                return false;
            }

        }
        public bool GetDeleteMenuMaster(int id)
    {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {
                
                _unitOfWork.MenuMasterRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }
    
     
        public List<MenuMasterEntity> Mapping(List<MenuMaster> deal)
        {
            List<MenuMasterEntity> lstMenuMaster = new List<MenuMasterEntity>();
            foreach (var item in deal)
            {
                MenuMasterEntity objMenuMaster = new MenuMasterEntity();
                objMenuMaster.Id = item.Id;
                objMenuMaster.MenuName = item.MenuName;
                objMenuMaster.RouterLink = item.RouterLink;
                objMenuMaster.Icon = item.Icon;
                objMenuMaster.Class = item.Class;
                objMenuMaster.MenuOrder = item.MenuOrder;

                lstMenuMaster.Add(objMenuMaster);
            }
            return lstMenuMaster;
        }

        public MenuMasterEntity MappingSingleObject(MenuMaster item)
        {
            MenuMasterEntity objMenuMaster = new MenuMasterEntity();
            objMenuMaster.Id = item.Id;
            objMenuMaster.MenuName = item.MenuName;
            objMenuMaster.RouterLink = item.RouterLink;
            objMenuMaster.Icon = item.Icon;
            objMenuMaster.Class = item.Class;
            objMenuMaster.MenuOrder = item.MenuOrder;

            return objMenuMaster;
        }
    }
}
