﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class InventoryServices : IInventoryServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public InventoryServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public InventoryEntity GetByInventoryId(int InventoryId)
        {
            var Inventorys = _unitOfWork.InventoryRepository.GetByID(InventoryId);
            if (Inventorys != null)
            {
                return MappingSingleObject(Inventorys);
            }
            return new InventoryEntity();
        }

        public IEnumerable<InventoryEntity> GetAllInventorys()
        {
            var Inventorys = _unitOfWork.InventoryRepository.GetAll().ToList();
            if (Inventorys.Any())
            {
                List<InventoryEntity> lstInventory =  Mapping(Inventorys);
                if (lstInventory.Count > 0)
                    return lstInventory.OrderByDescending(x => x.InventoryId).ToList();
                return lstInventory;
            }
            return new List<InventoryEntity>();
        }

        public List<InventoryEntity> GetAllInventorysByProductId(int productId, int distId)
        {
            var Inventorys = _unitOfWork.InventoryRepository.GetAll().Where(x=>x.ProductId == productId && x.DistributorId == distId).ToList();
            if (Inventorys.Any())
            {
                List<InventoryEntity> lstInventory = Mapping(Inventorys);
                if (lstInventory.Count > 0)
                    return lstInventory.OrderByDescending(x => x.InventoryId).ToList();
                return lstInventory;
            }
            return new List<InventoryEntity>();
        }

        public InventoryEntity CreateInventory(InventoryEntity InventoryEntity)
        {
            using (var scope = new TransactionScope())
            {
                InventoryEntity InventoryModel = new InventoryEntity();
                var inventory = _unitOfWork.InventoryRepository.GetAll().Where(x => x.ProductId == InventoryEntity.ProductId && x.DistributorId == InventoryEntity.DistributorId).FirstOrDefault();
                if(inventory == null)
                {
                    var Inventory = new Inventory
                    {
                        InventoryId = InventoryEntity.InventoryId,
                        ProductId = InventoryEntity.ProductId,
                        DistributorId = InventoryEntity.DistributorId,
                        Units = InventoryEntity.Units,
                        DistributorPrice = InventoryEntity.DistributorPrice,
                        DeliveredOn = InventoryEntity.DeliveredOn,
                        CreatedBy = InventoryEntity.CreatedBy,
                        CreatedDate = DateTime.Now,

                    };
                    _unitOfWork.InventoryRepository.Insert(Inventory);
                    _unitOfWork.Save();
                    var Inventorys = _unitOfWork.InventoryRepository.GetByID(Inventory.InventoryId);
                     InventoryModel = MappingSingleObject(Inventorys);
                }
                else
                {
                    inventory.Units = inventory.Units + InventoryEntity.Units;

                    _unitOfWork.InventoryRepository.Update(inventory);
                    _unitOfWork.Save();
                    var Inventorys = _unitOfWork.InventoryRepository.GetByID(inventory.InventoryId);
                     InventoryModel = MappingSingleObject(Inventorys);
                }
                scope.Complete();
                return InventoryModel;
            }
        }
        public bool GetDeleteInventory(int id)
        {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {

                _unitOfWork.InventoryRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }

        public bool UpdateInventory(InventoryEntity InventoryEntity)
        {
            var success = false;
            if (InventoryEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var Inventory = _unitOfWork.InventoryRepository.GetByID(InventoryEntity.InventoryId);
                    if (Inventory != null)
                    {
                        Inventory.InventoryId = InventoryEntity.InventoryId;
                        Inventory.ProductId = InventoryEntity.ProductId;
                        Inventory.DistributorId = InventoryEntity.DistributorId;
                        Inventory.Units = InventoryEntity.Units;
                        Inventory.DistributorPrice = InventoryEntity.DistributorPrice;
                        Inventory.DeliveredOn = InventoryEntity.DeliveredOn;

                        _unitOfWork.InventoryRepository.Update(Inventory);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;


                    }
                }
            }
            return success;
        }
        public InventoryEntity GetByProductId(int productId)
        {
            var Inventorys = _context.Inventories.Where(x => x.ProductId == productId).FirstOrDefault();
            if (Inventorys != null)
            {
                return MappingSingleObject(Inventorys);
            }
            return new InventoryEntity();
        }

        public List<InventoryEntity> Mapping(List<Inventory> deal)
        {
            List<InventoryEntity> lstInventory = new List<InventoryEntity>();
            foreach (var item in deal)
            {
                InventoryEntity objInventory = new InventoryEntity();
                objInventory.InventoryId = item.InventoryId;
                objInventory.DistributorId = item.DistributorId;
                objInventory.ProductId = item.ProductId;
                objInventory.DistributorPrice = item.DistributorPrice;
                objInventory.Units = item.Units;
                objInventory.DeliveredOn = item.DeliveredOn;
                objInventory.CreatedBy = item.CreatedBy;
                objInventory.CreatedDate = item.CreatedDate;
               

                MstProduct mstProduct = _unitOfWork.MstProductRepository.GetByID(objInventory.ProductId);
                MstProductEntity objProduct = new MstProductEntity();
                objProduct.ProductId = mstProduct.ProductId;
                objProduct.ProductName = mstProduct.ProductName;
                objProduct.Manufacturer = mstProduct.Manufacturer;
                objProduct.MRP = mstProduct.MRP;

                objInventory.MstProduct = objProduct;

                MstDistributor mstDistributor = _unitOfWork.MstDistributorRepository.GetByID(objInventory.DistributorId);
                MstDistributorEntity objDistributor = new MstDistributorEntity();
                objDistributor.DistributorName = mstDistributor.DistributorName;
                objDistributor.DistributorId = mstDistributor.DistributorId;
                objDistributor.Code = mstDistributor.Code;
                objDistributor.Address = mstDistributor.Address;
                objDistributor.PhoneNumber = mstDistributor.PhoneNumber;
                objDistributor.ContactPerson = mstDistributor.ContactPerson;
                objDistributor.CreatedBy = mstDistributor.CreatedBy;
                objDistributor.CreatedDate = mstDistributor.CreatedDate;
                objDistributor.ModifiedBy = mstDistributor.ModifiedBy;
                objDistributor.ModifiedDate = mstDistributor.ModifiedDate;

                objInventory.MstDistributor = objDistributor;

                lstInventory.Add(objInventory);
            }
            return lstInventory;
        }

        public InventoryEntity MappingSingleObject(Inventory item)
        {

            InventoryEntity objInventory = new InventoryEntity();
            objInventory.InventoryId = item.InventoryId;
            objInventory.DistributorId = item.DistributorId;
            objInventory.ProductId = item.ProductId;
            objInventory.DistributorPrice = item.DistributorPrice;
            objInventory.Units = item.Units;
            objInventory.DeliveredOn = item.DeliveredOn;
            objInventory.CreatedBy = item.CreatedBy;
            objInventory.CreatedDate = item.CreatedDate;


            MstProduct mstProduct = _unitOfWork.MstProductRepository.GetByID(objInventory.ProductId);
            MstProductEntity objProduct = new MstProductEntity();
            objProduct.ProductId = mstProduct.ProductId;
            objProduct.ProductName = mstProduct.ProductName;
            objProduct.Manufacturer = mstProduct.Manufacturer;
            objProduct.MRP = mstProduct.MRP;

            objInventory.MstProduct = objProduct;

            MstDistributor mstDistributor = _unitOfWork.MstDistributorRepository.GetByID(objInventory.DistributorId);
            MstDistributorEntity objDistributor = new MstDistributorEntity();
            objDistributor.DistributorName = mstDistributor.DistributorName;
            objDistributor.DistributorId = mstDistributor.DistributorId;
            objDistributor.Code = mstDistributor.Code;
            objDistributor.Address = mstDistributor.Address;
            objDistributor.PhoneNumber = mstDistributor.PhoneNumber;
            objDistributor.ContactPerson = mstDistributor.ContactPerson;
            objDistributor.CreatedBy = mstDistributor.CreatedBy;
            objDistributor.CreatedDate = mstDistributor.CreatedDate;
            objDistributor.ModifiedBy = mstDistributor.ModifiedBy;
            objDistributor.ModifiedDate = mstDistributor.ModifiedDate;

            objInventory.MstDistributor = objDistributor;

            return objInventory;
        }
    }
}
