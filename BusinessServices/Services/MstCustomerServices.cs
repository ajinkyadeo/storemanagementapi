﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstCustomerServices : IMstCustomerServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstCustomerServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public MstCustomerEntity GetByCustomerId(int CustomerId)
        {
            var Customers = _unitOfWork.MstCustomerRepository.GetByID(CustomerId);
            if (Customers != null)
            {
                return MappingSingleObject(Customers);
            }
            return new MstCustomerEntity();
        }

        public IEnumerable<MstCustomerEntity> GetAllCustomers()
        {
            var Customers = _unitOfWork.MstCustomerRepository.GetAll().ToList();
            if (Customers.Any())
            {
                List<MstCustomerEntity> lstCustomer =  Mapping(Customers);
                if (lstCustomer.Count > 0)
                    return lstCustomer.OrderByDescending(x => x.CustomerName).ToList();
                return lstCustomer;
            }
            return new List<MstCustomerEntity>();
        }

        public MstCustomerEntity CreateCustomer(MstCustomerEntity CustomerEntity)
        {
            using (var scope = new TransactionScope())
            {
                var Customer = new MstCustomer
                {
                    CustomerName = CustomerEntity.CustomerName,
                    PhoneNumber = CustomerEntity.PhoneNumber,
                    Email = CustomerEntity.Email,

                };
                _unitOfWork.MstCustomerRepository.Insert(Customer);
                _unitOfWork.Save();

                var Customers = _unitOfWork.MstCustomerRepository.GetByID(Customer.CustomerId);
                MstCustomerEntity mstCustomerModel = MappingSingleObject(Customers);
                scope.Complete();
                return mstCustomerModel;
            }
        }
        public bool GetDeleteCustomer(int id)
        {
            bool isSuccess = false;
            using (var scope = new TransactionScope())
            {

                _unitOfWork.MstCustomerRepository.Delete(id);
                _unitOfWork.Save();
                scope.Complete();
                isSuccess = true;
                return isSuccess;
            }
        }

        public bool UpdateCustomer(MstCustomerEntity CustomerEntity)
        {
            var success = false;
            if (CustomerEntity != null)
            {
                using (var scope = new TransactionScope())
                {
                    var Customer = _unitOfWork.MstCustomerRepository.GetByID(CustomerEntity.CustomerId);
                    if (Customer != null)
                    {
                        Customer.CustomerName = CustomerEntity.CustomerName;
                        Customer.PhoneNumber = CustomerEntity.PhoneNumber;

                        _unitOfWork.MstCustomerRepository.Update(Customer);
                        _unitOfWork.Save();

                        scope.Complete();
                        success = true;
                    }
                }
            }
            return success;
        }
        public List<MstCustomerEntity> Mapping(List<MstCustomer> deal)
        {
            List<MstCustomerEntity> lstCustomer = new List<MstCustomerEntity>();
            foreach (var item in deal)
            {
                MstCustomerEntity objCustomer = new MstCustomerEntity();
                objCustomer.CustomerId = item.CustomerId;
                objCustomer.CustomerName = item.CustomerName;
                objCustomer.PhoneNumber = item.PhoneNumber;
                objCustomer.Email = item.Email;
                lstCustomer.Add(objCustomer);
            }
            return lstCustomer;
        }

        public MstCustomerEntity MappingSingleObject(MstCustomer item)
        {

            MstCustomerEntity objCustomer = new MstCustomerEntity();
            objCustomer.CustomerId = item.CustomerId;
            objCustomer.CustomerName = item.CustomerName;
            objCustomer.PhoneNumber = item.PhoneNumber;
            objCustomer.Email = item.Email;
            return objCustomer;
        }
    }
}
