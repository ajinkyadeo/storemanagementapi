﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AutoMapper;
using BusinessEntities;
using DataModel;
using DataModel.UnitOfWork;

namespace BusinessServices
{
    /// <summary>
    /// Offers services for product specific CRUD operations
    /// </summary>
    public class MstDistributorServices : IMstDistributorServices
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly StoreManagementEntities _context = new StoreManagementEntities();

        public MstDistributorServices(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public BusinessEntities.MstDistributorEntity GetById(int id)
        {
            var property = _unitOfWork.MstDistributorRepository.GetByID(id);
            if (property != null)
            {
                return MappingSingleObject(property);
            }
            return new MstDistributorEntity();
        }

        public IEnumerable<MstDistributorEntity> GetAllDistributor()
        {
            List<MstDistributorEntity> lstUser = new List<MstDistributorEntity>();
            var property = _unitOfWork.MstDistributorRepository.GetAll().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.DistributorName);
                return lstUser;
            }
            return lstUser;
        }


        public IEnumerable<MstDistributorEntity> GetAllDistributorByProduct(int productId)
        {
            List<MstDistributorEntity> lstUser = new List<MstDistributorEntity>();
            var property = (from p in _context.MstProducts
                            join ip in _context.Inventories on p.ProductId equals ip.ProductId
                            join d in _context.MstDistributors on ip.DistributorId equals d.DistributorId
                            where p.ProductId == productId
                            select d
                    ).Distinct().ToList();
            if (property.Any())
            {
                lstUser = Mapping(property);
                if (lstUser.Count > 0)
                    return lstUser.OrderBy(x => x.DistributorName);
                return lstUser;
            }
            return lstUser;
        }

        public List<MstDistributorEntity> Mapping(List<MstDistributor> prop)
        {
            List<MstDistributorEntity> lstDistributor = new List<MstDistributorEntity>();
            foreach (var item in prop)
            {
                MstDistributorEntity objDistributor = new MstDistributorEntity();
                objDistributor.DistributorName = item.DistributorName;
                objDistributor.DistributorId = item.DistributorId;
                objDistributor.Code = item.Code;
                objDistributor.Address = item.Address;
                objDistributor.PhoneNumber = item.PhoneNumber;
                objDistributor.ContactPerson = item.ContactPerson;
                objDistributor.CreatedBy = item.CreatedBy;
                objDistributor.CreatedDate = item.CreatedDate;
                objDistributor.ModifiedBy = item.ModifiedBy;
                objDistributor.ModifiedDate = item.ModifiedDate;

                lstDistributor.Add(objDistributor);
            }
            return lstDistributor;
        }

        public MstDistributorEntity MappingSingleObject(MstDistributor item)
        {
            MstDistributorEntity objDistributor = new MstDistributorEntity();
            objDistributor.DistributorName = item.DistributorName;
            objDistributor.DistributorId = item.DistributorId;
            objDistributor.Code = item.Code;
            objDistributor.Address = item.Address;
            objDistributor.PhoneNumber = item.PhoneNumber;
            objDistributor.ContactPerson = item.ContactPerson;
            objDistributor.CreatedBy = item.CreatedBy;
            objDistributor.CreatedDate = item.CreatedDate;
            objDistributor.ModifiedBy = item.ModifiedBy;
            objDistributor.ModifiedDate = item.ModifiedDate;
            return objDistributor;
        }
    }
}
