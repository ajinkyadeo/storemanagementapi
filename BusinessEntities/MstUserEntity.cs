﻿using System;

namespace BusinessEntities
{
    public class MstUserEntity
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }
        public int RoleId { get; set; }
        public DateTime DOB { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int?  ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool Lock { get; set; }

        public virtual MstRoleEntity MstRole { get; set; }
      
    }
}
