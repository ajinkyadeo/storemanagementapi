﻿using System;

namespace BusinessEntities
{
    public class RoleMenuEntity
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public bool AddPermission { get; set; }
        public bool ViewPermission { get; set; }
        public bool DeletePermission { get; set; }
        public bool EditPermission { get; set; }

        public virtual MenuMasterEntity menus { get; set; }
        //public virtual MstRoleEntity Role { get; set; }
        //public virtual List<MenuMasterEntity> MenuMaster { get; set; }
    }
}
