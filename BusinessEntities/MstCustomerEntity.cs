﻿using System;

namespace BusinessEntities
{
    public class MstCustomerEntity
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

    }
}
