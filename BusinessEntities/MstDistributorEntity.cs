﻿using System;

namespace BusinessEntities
{
    public class MstDistributorEntity
    {
        public int DistributorId { get; set; }
        public string Code { get; set; }
        public string DistributorName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string ContactPerson { get; set; }
        public int? CreatedBy { get; set; }
        public  DateTime? CreatedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

      
    }
}
