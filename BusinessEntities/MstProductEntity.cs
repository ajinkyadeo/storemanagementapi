﻿using System;

namespace BusinessEntities
{
    public class MstProductEntity
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Manufacturer { get; set; }
        public string MRP { get; set; }
      
    }
}
