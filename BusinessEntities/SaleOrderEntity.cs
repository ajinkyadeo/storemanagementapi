﻿using System;

namespace BusinessEntities
{
    public class SaleOrderEntity
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public int ProductId { get; set; }
        public int DistributorId { get; set; }
        public string DiscountVal { get; set; }
        public string DiscountPer { get; set; }
        public string MRP { get; set; }
        public string Price { get; set; }

        public virtual MstCustomerEntity MstCustomer { get; set; }
        public virtual MstDistributorEntity MstDistributor { get; set; }
        public virtual MstProductEntity MstProduct { get; set; }


    }
}
