﻿using System;

namespace BusinessEntities
{
    public class InventoryEntity
    {
        public int InventoryId { get; set; }
        public int ProductId { get; set; }
        public int DistributorId { get; set; }
        public int Units { get; set; }
        public string DistributorPrice { get; set; }
        public DateTime DeliveredOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public virtual MstDistributorEntity MstDistributor { get; set; }
        public virtual MstProductEntity MstProduct { get; set; }


    }
}
