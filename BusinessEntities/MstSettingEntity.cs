﻿using System;

namespace BusinessEntities
{
    public class MstSettingEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
