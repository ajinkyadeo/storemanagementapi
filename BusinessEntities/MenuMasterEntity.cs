﻿using System;
using System.Collections.Generic;

namespace BusinessEntities
{
    public class MenuMasterEntity
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public string RouterLink { get; set; }
        public string Icon { get; set; }
        public string Class { get; set; }
        public int? MenuOrder { get; set; }



    }
}

